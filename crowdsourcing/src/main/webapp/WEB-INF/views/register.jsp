<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<jsp:include page="header.jsp">
	<jsp:param name="title" value="Register" />
</jsp:include>
 
<c:if test="${not empty error}">
	<div class="alert alert-danger">
		<b>Error</b> This username is already taken or empty. Please choose another one.
	</div>
</c:if>

<form name='f' action="register" method='POST'>

	<table>
		<tr>
			<th>Username</th>
			<td>
				<input type='text' name='username' value=''>
			</td>
		</tr>
		<tr>
			<th>Password</th>
			<td>
				<input type='password' name='password' />
			</td>
		</tr>
		<tr>
			<td colspan='2'><br>
				<input class="btn btn-primary" name="submit" type="submit" value="Register" />
				<input class="btn" name="reset" type="reset" value="Reset" />
			</td>
		</tr>
	</table>
	
</form>
	
<jsp:include page="footer.jsp" />