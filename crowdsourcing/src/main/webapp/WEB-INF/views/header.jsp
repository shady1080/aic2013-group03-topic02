<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/style.css" />" />
	<title><c:out value="${param.title}" /> - Crowdsourcing</title>
	
	<!-- Bootstrap core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="${pageContext.servletContext.contextPath}">Crowdsourcing</a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li<c:if test="${param.menuActive eq \"tasks\"}"> class="active"</c:if>>
      	<a href="${pageContext.servletContext.contextPath}/tasks?page=0">Tasks</a>
      </li>
      <li<c:if test="${param.menuActive eq \"earnings\"}"> class="active"</c:if>>
      	<a href="${pageContext.servletContext.contextPath}/earnings">Earnings</a>
      </li>
    </ul>
    <sec:authorize ifAnyGranted="ROLE_USER">
	    <ul class="nav navbar-nav navbar-right">
	      <li><a href="${pageContext.servletContext.contextPath}/j_spring_security_logout">Logout</a></li>
	    </ul>
	</sec:authorize>
	<sec:authorize ifAllGranted="ROLE_ANONYMOUS">
	    <ul class="nav navbar-nav navbar-right">
	    <li><a href="${pageContext.servletContext.contextPath}/register">Register</a></li>
	      <li><a href="${pageContext.servletContext.contextPath}/login">Login</a></li>
	    </ul>
	</sec:authorize>
  </div><!-- /.navbar-collapse -->
</nav>
