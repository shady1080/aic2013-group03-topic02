<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jsp:include page="header.jsp">
	<jsp:param name="title" value="Tasks" />
	<jsp:param name="menuActive" value="task" />
</jsp:include>

<h2>Task #<c:out value="${task.taskID}"/></h2>
<p class="task">${task.taskDescription}</p>
<form:form modelAttribute="task" method="POST" action="${task.taskID}">
	<input id="userID" name="userID" type="hidden" value="<c:out value="${user.id}" />" />
	<form:radiobuttons path="answer" items="${answerPossibilities}" />
	<input name="taskID" type="hidden" value="${task.taskID}" />
	<input class="btn btn-primary" type="submit" value="Submit" />
</form:form>

<jsp:include page="footer.jsp" />
