<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<jsp:include page="header.jsp">
	<jsp:param name="title" value="Login" />
</jsp:include>

<c:if test="${not empty error}">
	<div class="alert alert-danger">
		<b>Error</b> Wrong login details provided, or your account has been blocked.
	</div>
</c:if>

<form name='f' action="<c:url value='j_spring_security_check' />" method='POST'>

	<table>
		<tr>
			<th>Username</th>
			<td><input type='text' name='j_username' value=''>
			</td>
		</tr>
		<tr>
			<th>Password</th>
			<td><input type='password' name='j_password' />
			</td>
		</tr>
		<tr>
			<td colspan='2'><br>
				<input class="btn btn-primary" name="submit" type="submit" value="Login" />
				<input class="btn" name="reset" type="reset" value="Reset" />
			</td>
		</tr>
	</table>
</form>
	
<jsp:include page="footer.jsp" />