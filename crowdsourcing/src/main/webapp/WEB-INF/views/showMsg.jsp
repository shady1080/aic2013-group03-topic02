<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<jsp:include page="header.jsp">
	<jsp:param name="title" value="Register" />
</jsp:include>

<h1><c:out value="${title}"/></h1>
<p><c:out value="${msg}"/></p>

<c:choose>
     <c:when test="${logout==true}">
     	<a href="${pageContext.servletContext.contextPath}/j_spring_security_logout"><button>Logout</button></a>
     </c:when>

     <c:otherwise>
     	<a href="${pageContext.servletContext.contextPath}/tasks?page=0"><button>Go Back</button></a>
     </c:otherwise>
</c:choose>

<jsp:include page="footer.jsp" />