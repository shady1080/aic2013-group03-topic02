<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<jsp:include page="header.jsp">
	<jsp:param name="title" value="Tasks" />
	<jsp:param name="menuActive" value="tasks" />
</jsp:include>

<c:if test="${fn:length(undoneTasks) ne 0}">
	<ul class="pager">
	  	<c:choose>
		   	<c:when test="${page eq 0}">
		   		<li class="disabled"><a>Previous</a></li>
		   	</c:when>
		    <c:otherwise>
		   		<li><a href="${pageContext.servletContext.contextPath}/tasks?page=${page-1}">Previous</a></li>
		    </c:otherwise>
	 	</c:choose>
	 	
	 	<c:choose>
		   	<c:when test="${fn:length(undoneTasks) < 50}">
		   		<li class="disabled"><a>Next</a></li>
		   	</c:when>
		    <c:otherwise>
		   		<li><a href="${pageContext.servletContext.contextPath}/tasks?page=${page+1}">Next</a></li>
		    </c:otherwise>
	 	</c:choose>
	</ul>
</c:if>
		
<c:choose>
	<c:when test="${empty undoneTasks}">
		<h3>No tasks found.</h3>
	</c:when>
	
	<c:otherwise>
		<div class="list-group">
		<c:forEach var="task" items="${undoneTasks}" >
			<a href="tasks/${task.taskID}" class="list-group-item">
			 	<h4 class="list-group-item-heading">Task #<c:out value="${task.taskID}" /></h4>
				<p class="list-group-item-text"><c:out value="${task.taskDescription}" /></p>
				<span class="price"><c:out value="${task.price}" /> cent</span>
			</a>
		</c:forEach>
		</div>
   </c:otherwise>
</c:choose>

<jsp:include page="footer.jsp" />