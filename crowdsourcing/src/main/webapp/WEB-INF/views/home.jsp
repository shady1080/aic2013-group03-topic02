<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<jsp:include page="header.jsp">
	<jsp:param name="title" value="Home" />
</jsp:include>

<div class="jumbotron">
  <h1>Be a worker and get rich!</h1>
  <p>Solve some tasks and earn reward for it. We have currently ${undoneTasks} tasks in the crowd!</p>
  <p>If you are new here <a href="<c:url value="register" />">register</a> for a new account
  or <a href="<c:url value="login" />">login</a> with your user credentials.</p>
  <p><a class="btn btn-primary btn-lg" role="button" href="<c:url value="tasks" />">Earn money</a></p>
</div>

<jsp:include page="footer.jsp" />