<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<jsp:include page="header.jsp">
	<jsp:param name="title" value="Earnings" />
	<jsp:param name="menuActive" value="earnings" />
</jsp:include>

<p>
Hello <b>${username}</b>!
You have already solved <b>${numSolvedTasks}</b> task(s), which results in a total of <b>${totalReward}</b> cents reward.
</p>

<jsp:include page="footer.jsp" />