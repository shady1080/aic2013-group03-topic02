package ac.tuwien.aic13g3t2.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Http {

	private static final Logger log = LoggerFactory.getLogger(Http.class);
	
	/*private static String perform(String requestMethod, String path,
			Map<String,String> data) throws IOException {
		
		URL url = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(requestMethod);
		conn.setRequestProperty("Content-Type", "application/json");
		
		if (data != null) { // construct json parameter string from data
			String json = "{";
			int size = data.entrySet().size();
			for (Entry<String,String> e : data.entrySet()) {
				json += "\"" + e.getKey() + "\":\"" + e.getValue() + "\"";
				if (size-- > 1) {
					json += ",";
				}
	        }
			json += "}";
			log.debug(path + " // " + json);
			
			OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
			out.write(json);
			out.close();
		}
		
		/* receive response *
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = null;
        String response = "";
        while ((line = rd.readLine()) != null) {
        	response += line;
        }
        rd.close();
        
        return response;
		
	}
	
	public static String put(String path, Map<String,String> data) throws IOException {
		return perform("PUT", path, data);
	}
	
	public static String post(String path, Map<String,String> data) throws IOException {
		return perform("POST", path, data);
	}
	
	public static String get(String path) throws IOException {
		return perform("GET", path, null);
	}
	
	public static String get(String path, Map<String,String> data) throws IOException {
		return perform("UPDATE", path, data);
	}*/
	
	
	public static String put(String path, Map<String,String> data)
			throws IOException {
		
		/* construct json parameter string from data */
		String json = "{";
		int size = data.entrySet().size();
		for (Entry<String,String> e : data.entrySet()) {
			json += "\"" + e.getKey() + "\":\"" + e.getValue() + "\"";
			if (size-- > 1) {
				json += ",";
			}
        }
		json += "}";
		log.debug(path + " // " + json);
		
		/* send request */
		URL url = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("PUT");
		conn.setRequestProperty("Content-Type", "application/json");
		
		OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
		out.write(json);
		out.close();
		
		/* receive response */
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = null;
        String response = "";
        while ((line = rd.readLine()) != null) {
        	response += line;
        }
        rd.close();
        
        return response;
	}
	
}
