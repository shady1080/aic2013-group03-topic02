package ac.tuwien.aic13g3t2.util;

public final class Settings {
	//public static String CROWDSOURCEURL = "http://5.175.172.128:8080/crowdsourcing";
	private static final String CROWDSOURCEURL = "http://localhost:8080/aic13g3t2-crowdsourcing";
	
	//public static String SENTIMENTURL = "http://84.200.10.39:8080/sentiment";
	public static final String SENTIMENTURL = "http://localhost:8080/aic13g3t2-sentiment";
	
}
