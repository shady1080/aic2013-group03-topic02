package ac.tuwien.aic13g3t2.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tasks")
public class Task {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long taskID;
	
	private Long remoteID;
	
	@Column(columnDefinition = "text")
	private String taskDescription;
	
	private String answerPossibilities;
	
	private Double price;
	
	private String answer;
	
	private Integer userID;
	
	private String callbackURL;
	
	private Boolean obsolete;

	public Task() {
	}

	public Long getTaskID() {
		return taskID;
	}

	public void setTaskID(Long taskID) {
		this.taskID = taskID;
	}
	
	public Long getRemoteID() {
		return remoteID;
	}

	public void setRemoteID(Long remoteID) {
		this.remoteID = remoteID;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public String getAnswerPossibilities() {
		return answerPossibilities;
	}

	public void setAnswerPossibilities(String answerPossibilities) {
		this.answerPossibilities = answerPossibilities;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "Task [taskID=" + taskID + ", remoteID=" + remoteID
				+ ", taskDescription=" + taskDescription
				+ ", answerPossibilities=" + answerPossibilities
				+ ", price=" + price + ", answer=" + answer + ", userID="
				+ userID + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((answer == null) ? 0 : answer.hashCode());
		result = prime
				* result
				+ ((answerPossibilities == null) ? 0 : answerPossibilities
						.hashCode());
		result = prime * result
				+ ((taskDescription == null) ? 0 : taskDescription.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result
				+ ((remoteID == null) ? 0 : remoteID.hashCode());
		result = prime * result + ((taskID == null) ? 0 : taskID.hashCode());
		result = prime * result + ((userID == null) ? 0 : userID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (answer == null) {
			if (other.answer != null)
				return false;
		} else if (!answer.equals(other.answer))
			return false;
		if (answerPossibilities == null) {
			if (other.answerPossibilities != null)
				return false;
		} else if (!answerPossibilities.equals(other.answerPossibilities))
			return false;
		if (taskDescription == null) {
			if (other.taskDescription != null)
				return false;
		} else if (!taskDescription.equals(other.taskDescription))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (remoteID == null) {
			if (other.remoteID != null)
				return false;
		} else if (!remoteID.equals(other.remoteID))
			return false;
		if (taskID == null) {
			if (other.taskID != null)
				return false;
		} else if (!taskID.equals(other.taskID))
			return false;
		if (userID == null) {
			if (other.userID != null)
				return false;
		} else if (!userID.equals(other.userID))
			return false;
		return true;
	}

	public String getCallbackURL() {
		return callbackURL;
	}

	public void setCallbackURL(String callbackURL) {
		this.callbackURL = callbackURL;
	}

	public Boolean isObsolete() {
		return obsolete;
	}

	public void setObsolete(Boolean obsolete) {
		this.obsolete = obsolete;
	}
}
