package ac.tuwien.aic13g3t2.service;

import javassist.NotFoundException;

public interface ICrowdServerService {

	void submitTask(Long remoteID,
					String taskDescription,
					String answerPossibilities,
					Double price,
					String callbackUri) throws NotFoundException;
	
}
