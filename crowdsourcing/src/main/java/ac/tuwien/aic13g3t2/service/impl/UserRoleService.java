package ac.tuwien.aic13g3t2.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ac.tuwien.aic13g3t2.dao.IUserRoleDAO;
import ac.tuwien.aic13g3t2.entity.UserRole;
import ac.tuwien.aic13g3t2.service.IUserRoleService;

@Service("userRoleService")
public class UserRoleService implements IUserRoleService {
	
	@Autowired
    private IUserRoleDAO userRoleDAO;
	
	@Override
	public void add(UserRole entity) {
		userRoleDAO.add(entity);
	}

	@Override
	public void update(UserRole entity) {
		userRoleDAO.update(entity);
	}

	@Override
	public void remove(UserRole entity) {
		userRoleDAO.remove(entity);
	}

	@Override
	public UserRole find(Long key) {
		return userRoleDAO.find(key);
	}

	@Override
	public List<UserRole> list() {
		return userRoleDAO.list();
	}

	@Override
	public UserRole findByName(String rolename) {
		return userRoleDAO.findByName(rolename);
	}

}
