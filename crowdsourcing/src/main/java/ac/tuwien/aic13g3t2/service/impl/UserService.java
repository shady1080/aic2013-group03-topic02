package ac.tuwien.aic13g3t2.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ac.tuwien.aic13g3t2.dao.IUserDAO;
import ac.tuwien.aic13g3t2.entity.User;
import ac.tuwien.aic13g3t2.entity.UserDA;
import ac.tuwien.aic13g3t2.service.IUserService;

@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
@Service("userService")
public class UserService implements IUserService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
    private IUserDAO userDAO;
	
	@Override
	public List<UserDA> listBlocked() {
		List<UserDA> retUsers = new ArrayList<UserDA>();
		List<User> users = userDAO.listBlocked();
		
		for(User u : users){
			UserDA userDA = new UserDA();
			userDA.setId(u.getId());
			userDA.setUsername(u.getUsername());
			userDA.setActive(u.isActive());
			//userDA.setRoles(u.getRoles());
			retUsers.add(userDA);
		}
		return retUsers;
	}

	@Override
	public void banUser(Long id) {
		
		User banUser = userDAO.find(id);
		
		if(banUser != null){
			banUser.setActive(false);
		}
		
	}

	@Override
	public void add(User entity) {
		userDAO.add(entity);
	}

	@Override
	public void update(User entity) {
		userDAO.update(entity);
	}

	@Override
	public void remove(User entity) {
		userDAO.remove(entity);
	}

	@Override
	public User find(Long key) {
		return userDAO.find(key);
	}

	@Override
	public List<User> list() {
		return userDAO.list();
	}
	
	@Override
	public User findByName(String username) {
		return userDAO.findByName(username);
	}
	
}
