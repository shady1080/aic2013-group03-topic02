package ac.tuwien.aic13g3t2.service;

import java.util.List;

import ac.tuwien.aic13g3t2.entity.Task;

public interface ITaskService extends IGenericService<Task, Long> {

	List<Task> listUndone();

	List<Task> listDoneFromUser(Long id);

	Double totalRewardFromUser(Long id);

	List<Task> listUndoneNotObsolete();

	List<Task> listUndoneFromUser(Long userID, int page);

}
