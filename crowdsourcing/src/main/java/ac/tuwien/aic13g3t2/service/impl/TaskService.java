package ac.tuwien.aic13g3t2.service.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ac.tuwien.aic13g3t2.dao.ITaskDAO;
import ac.tuwien.aic13g3t2.entity.Task;
import ac.tuwien.aic13g3t2.service.ITaskService;

@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
@Service("taskService")
public class TaskService implements ITaskService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
    private ITaskDAO taskDAO;
	
	public void add(Task entity) {
		taskDAO.add(entity);		
	}

	public void update(Task entity) {
		taskDAO.update(entity);
	}

	public void remove(Task entity) {
		taskDAO.remove(entity);
	}

	public Task find(Long key) {
		return taskDAO.find(key);
	}

	public List<Task> list() {
		return taskDAO.list();
	}

	@Override
	public List<Task> listUndoneFromUser(Long userID, int page) {
		return taskDAO.listUndoneFromUser(userID, page);
	}

	@Override
	public List<Task> listUndone() {
		return taskDAO.listUndone();
	}

	@Override
	public List<Task> listDoneFromUser(Long userID) {
		return taskDAO.listDoneFromUser(userID);
	}

	@Override
	public Double totalRewardFromUser(Long userID) {
		return taskDAO.totalRewardFromUser(userID);
	}

	@Override
	public List<Task> listUndoneNotObsolete() {
		return taskDAO.listUndoneNotObsolete();
	}

}
