package ac.tuwien.aic13g3t2.service;

import java.util.List;

import ac.tuwien.aic13g3t2.entity.User;
import ac.tuwien.aic13g3t2.entity.UserDA;

public interface IUserService extends IGenericService<User, Long> {
	
	List<UserDA> listBlocked();
	void banUser(Long id);
	User findByName(String username);
	
}
