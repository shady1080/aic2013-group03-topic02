package ac.tuwien.aic13g3t2.service.impl;

import javassist.NotFoundException;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ac.tuwien.aic13g3t2.dao.ITaskDAO;
import ac.tuwien.aic13g3t2.entity.Task;
import ac.tuwien.aic13g3t2.service.ICrowdServerService;

@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
@Service("submitTask")
public class CrowdServerService implements ICrowdServerService {
	
	private static final Logger log = LoggerFactory.getLogger(CrowdServerService.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
    private ITaskDAO taskDAO;

	@Override
	public void submitTask(Long remoteID, String taskDescription, String answerPossibilities,
			Double price, String callbackURL) throws NotFoundException {
		
		Task task = new Task();
		task.setRemoteID(remoteID);
		task.setAnswerPossibilities(answerPossibilities);
		task.setTaskDescription(taskDescription);
		task.setPrice(price);
		task.setCallbackURL(callbackURL);
		task.setObsolete(false);
		
		taskDAO.add(task);
		log.info("New task successfully submitted to the crowd.");
	}
	
}
