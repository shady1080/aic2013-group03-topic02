package ac.tuwien.aic13g3t2.service;

import ac.tuwien.aic13g3t2.entity.UserRole;

public interface IUserRoleService extends IGenericService<UserRole, Long> {
	
	UserRole findByName(String rolename);
	
}
