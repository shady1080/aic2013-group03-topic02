package ac.tuwien.aic13g3t2.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javassist.NotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ac.tuwien.aic13g3t2.entity.Task;
import ac.tuwien.aic13g3t2.entity.UserDA;
import ac.tuwien.aic13g3t2.service.ICrowdServerService;
import ac.tuwien.aic13g3t2.service.ITaskService;
import ac.tuwien.aic13g3t2.service.IUserService;

@Controller
public class SentimentController {
	
	private static final Logger log = LoggerFactory.getLogger(SentimentController.class);
	
	@Autowired
	private ICrowdServerService crowdServerService;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private ITaskService taskService;
	
	@RequestMapping(value = "tasks", method = RequestMethod.POST)
	public void submitTask(@RequestParam(value="ratingID") Long remoteID,
			@RequestParam(value="taskDescription") String taskDescription,
			@RequestParam(value="answerPossibilities") String answerPossibilities,
			@RequestParam(value="price") Double price, 
			@RequestParam(value="callbackURL") String callbackURL)
					throws IOException, NotFoundException {
		crowdServerService.submitTask(remoteID, taskDescription, answerPossibilities, price, callbackURL);
	}
	
	@RequestMapping(value = "tasks/{taskID}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteTask(@PathVariable Long taskID) throws IOException {
		Task taskToDelete = taskService.find(taskID);
		if (taskToDelete != null)
			taskToDelete.setObsolete(true);
			taskService.update(taskToDelete);
	}
	
	@RequestMapping(value = "tasks/{taskID}", method = RequestMethod.PUT)
	@ResponseBody
	public void updateTaskPrice(@PathVariable Long taskID,
			@RequestBody String body) throws IOException {
		//log.info(body);
		Task taskToUpdate = taskService.find(taskID);
		if (taskToUpdate != null) {
			taskToUpdate.setPrice(Double.parseDouble(body));
			taskToUpdate.setObsolete(false);
			taskService.update(taskToUpdate);
		}
	}
	
	/* return a map containing all undone tasks */
	@RequestMapping(value = "undonetasks", method = RequestMethod.GET)
	@ResponseBody
	public Map<Long,Task> getTaskPrices(Locale locale, Model model) throws IOException {
		List<Task> undoneTasks = taskService.listUndone();
		Map<Long,Task> tasks = new HashMap<Long,Task>();
		for (Task t : undoneTasks) {
			tasks.put(t.getRemoteID(), t);
		}
		return tasks;
	}
	
	/* return a map containing all undone tasks that are not obsolete */
	@RequestMapping(value = "numundonetasks", method = RequestMethod.GET)
	@ResponseBody
	public Integer getTaskPricesOfNotObseleteTasks(Locale locale, Model model) throws IOException {
		return taskService.listUndoneNotObsolete().size();
	}
	
	@RequestMapping(value = "bans/{userID}", method = RequestMethod.POST)
	@ResponseBody
	public void banUser(@PathVariable Long userID, Locale locale, Model model) throws IOException {	
		userService.banUser(userID);
	}
	
	@RequestMapping(value = "bans", method = RequestMethod.GET)
	@ResponseBody
	public List<UserDA> listBlocked(Locale locale, Model model) throws IOException {		
		return userService.listBlocked();
	}
	
}
