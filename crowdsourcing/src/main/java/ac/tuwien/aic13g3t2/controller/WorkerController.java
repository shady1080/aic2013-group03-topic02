package ac.tuwien.aic13g3t2.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ac.tuwien.aic13g3t2.entity.Task;
import ac.tuwien.aic13g3t2.entity.User;
import ac.tuwien.aic13g3t2.entity.UserRole;
import ac.tuwien.aic13g3t2.entity.UserRole.Authority;
import ac.tuwien.aic13g3t2.service.ITaskService;
import ac.tuwien.aic13g3t2.service.IUserRoleService;
import ac.tuwien.aic13g3t2.service.IUserService;
import ac.tuwien.aic13g3t2.util.Helper;
import ac.tuwien.aic13g3t2.util.Http;

@Controller
public class WorkerController {

	private static final Logger log = LoggerFactory.getLogger(WorkerController.class);

	@Autowired
	private ITaskService taskService;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IUserRoleService userRoleService;
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public String printHome(ModelMap model, Principal principal ) {
		model.addAttribute("undoneTasks", taskService.listUndoneNotObsolete().size());
		return "home";
	}
 
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String login(ModelMap model) {
		return "login";
	}
 
	@RequestMapping(value="/loginfailed", method = RequestMethod.GET)
	public String loginerror(ModelMap model, Principal principal) {
		log.info("User tried to login without success.");
		model.addAttribute("error", "true");
		return "login";
	}
 
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logout(ModelMap model, Principal principal) {
		log.info("'" + principal.getName() + "' logged out successfully.");
		return "login";
	}
	
	@RequestMapping(value = "register", method = RequestMethod.GET)
	public String register(Locale locale, Model model) throws IOException {	
		return "register";
	}
	
	@RequestMapping(value = "register", method = RequestMethod.POST)
	public String registerNewUser(Locale locale, Model model,
			@RequestParam(value="username", required=true) String username,
			@RequestParam(value="password", required=true) String password) throws IOException {	
		if (userService.findByName(username) != null
				|| !validUsername(username)
				|| !validPassword(password)) {
			model.addAttribute("error", "true");
			log.error("Invalid user details provided.");
			return "register";
		}
		
		// register a new role for the user
		UserRole role = new UserRole();
		role.setAuthority(Authority.ROLE_USER);
		
		// register a new user
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setActive(true);
		
		role.setUser(user);
		List<UserRole> authorities = new ArrayList<UserRole>();
		user.setRoles(authorities);
		
		userService.add(user);
		userRoleService.add(role);
		
		log.info("'" + username + "' has been registered successfully.");
		return "login";
	}
	
	private boolean validPassword(String password) {
		return !password.isEmpty();
	}

	private boolean validUsername(String username) {
		return !username.isEmpty();
	}
	
	@RequestMapping(value = "earnings", method = RequestMethod.GET)
	public String earnings(ModelMap model, Principal principal) throws IOException {
		User u = userService.findByName(principal.getName());
		if (u.isActive()) {
			model.addAttribute("numSolvedTasks", taskService.listDoneFromUser(u.getId()).size());
			model.addAttribute("totalReward", Helper.round(taskService.totalRewardFromUser(u.getId()), 2));
			model.addAttribute("username", principal.getName());
			return "earnings";
		} else {
			model = blockedUser(principal.getName(), model);
			return "showMsg";
		}
	}
	
	@RequestMapping(value = "tasks", method = RequestMethod.GET)
	public String tasks(ModelMap model, Principal principal, @RequestParam(value="page") Integer page)
			throws IOException {
		User u = userService.findByName(principal.getName());
		if (u.isActive()) {
			if (page < 0) page = 0;
		    List<Task> undoneTasks = taskService
		    		.listUndoneFromUser(userService.findByName(principal.getName()).getId(), page);
			model.addAttribute("undoneTasks", undoneTasks);
			model.addAttribute("user", userService.findByName(principal.getName()));
			model.addAttribute("page", page);
			log.info("'" + principal.getName() + "' accessed the list of undone tasks.");
			return "tasks";	
		} else {
			model = blockedUser(principal.getName(), model);
			return "showMsg";
		}
	}
	
	private ModelMap blockedUser(String username, ModelMap model) {
		log.info("Blocked user '" + username + "' tried to access the list of undone tasks.");
		model = showMsg(model, "Error", "Your user account has been blocked!", true);
		return model;
	}
	
	@RequestMapping(value = "tasks/{taskID}", method = RequestMethod.GET)
	public String openUndoneTask(ModelMap model, Principal principal,
			@PathVariable Long taskID) throws IOException {
		User u = userService.findByName(principal.getName());
		if (u.isActive()) {
			Task task = taskService.find(taskID);
			if (task != null) {
				//Split string like: zero or more whitespace, a literal comma, zero or more whitespace
				List<String> answerPossibilities = Arrays.asList(task.getAnswerPossibilities()
						.split("\\s*,\\s*"));
				model.addAttribute("answerPossibilities", answerPossibilities);
				model.addAttribute("task", task);
				model.addAttribute("user", userService.findByName(principal.getName()));
				return "task";
			} else {
				model = showMsg(model, "Error", "Task #" + taskID + " not found!", false);
				return "showMsg";
			}
		} else {
			model = blockedUser(principal.getName(), model);
			return "showMsg";
		}
	}

	@RequestMapping(value = "tasks/{taskID}", method = RequestMethod.POST)
	public String sendAnsweredTask(ModelMap model, Principal principal,
			@PathVariable Long taskID,
			@RequestParam(value="userID", required=true) Integer userID,
			@RequestParam(value="answer") String answer) throws IOException {
		model.addAttribute("taskID", taskID);
		model.addAttribute("answer", answer);
		
		Task task = taskService.find(taskID);
		if (task != null) {
			if (task.getAnswer() == null) {
			
				/* update task with given answer in the database */
				task.setUserID(userID);
				task.setAnswer(answer);
				task.setObsolete(true);
				taskService.update(task);
				
				/* perform callback to sentiment server */
				if (task.getCallbackURL() != null && !task.getCallbackURL().isEmpty()) {
					Map<String,String> data = new HashMap<String,String>();
					data.put("userID", task.getUserID()+"");
					data.put("rating", answer);
					Http.put(task.getCallbackURL(), data);
				} else {
					model = showMsg(model, "Error", "Cannot perform callback due to empty callback url!", false);
					return "showMsg";
				}
			} else {
				model = showMsg(model, "Error", "Task #" + taskID + " has already been answered!", false);
				return "showMsg";
			}
		} else {
			model = showMsg(model, "Error", "Task #" + taskID + " not found!", false);
			return "showMsg";
		}
		model = showMsg(model, "Thank you for answering task " + taskID + "!",
				"Your answer has been submitted successfully.", false);
		return "showMsg";
	}
	
	private ModelMap showMsg(ModelMap model, String title, String msg, boolean logout) {
		log.info(msg);
		model.addAttribute("title", title);
		model.addAttribute("msg", msg);
		if (logout) model.addAttribute("logout", true);
		return model;
	}
}
