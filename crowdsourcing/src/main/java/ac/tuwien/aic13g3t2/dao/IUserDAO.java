package ac.tuwien.aic13g3t2.dao;

import java.util.List;

import ac.tuwien.aic13g3t2.entity.User;

public interface IUserDAO extends IGenericDAO<User, Long> {
	
	List<User> listBlocked();
	User findByName(String username);
	
}
