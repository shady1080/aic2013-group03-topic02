package ac.tuwien.aic13g3t2.dao.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ac.tuwien.aic13g3t2.dao.ITaskDAO;
import ac.tuwien.aic13g3t2.entity.Task;

@Repository
public class TaskDAO extends HibernateDAO<Task, Long> implements ITaskDAO {

	@Override
	public List<Task> listUndoneFromUser(Long userID, int page) {
		return currentSession().createSQLQuery("SELECT * FROM tasks t WHERE t.userID IS NULL AND "
			+ "t.taskDescription NOT IN (SELECT q.taskDescription FROM tasks q WHERE q.userID=" + userID
			+ ") AND (t.obsolete=False OR t.obsolete IS NULL) GROUP BY t.taskDescription "
			+ "ORDER BY t.price DESC, t.taskID ASC "
			+ "LIMIT " + (page * 50) + ", 50").addEntity(Task.class).list();
	}

	@Override
	public List<Task> listUndone() {
		return currentSession().createCriteria(Task.class)
				.add(Restrictions.isNull("answer")).list();
	}
	
	@Override
	public List<Task> listUndoneNotObsolete() {
		return currentSession().createCriteria(Task.class)
				.add(Restrictions.isNull("answer"))
				.add(Restrictions.eq("obsolete", false)).list();
	}

	@Override
	public List<Task> listDoneFromUser(Long userID) {
		return currentSession().createCriteria(Task.class)
				.add(Restrictions.eq("userID", userID.intValue())).list();
	}

	@Override
	public Double totalRewardFromUser(Long userID) {
		List<Double> totalReward = currentSession()
				.createQuery("SELECT SUM(t.price) FROM Task t WHERE userID=" + userID).list();
		if (totalReward.isEmpty() || totalReward.get(0) == null)
			return 0.0; // no tasks solved yet
		return totalReward.get(0);
	}

}
