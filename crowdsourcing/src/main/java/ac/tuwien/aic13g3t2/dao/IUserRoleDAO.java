package ac.tuwien.aic13g3t2.dao;

import ac.tuwien.aic13g3t2.entity.UserRole;

public interface IUserRoleDAO extends IGenericDAO<UserRole, Long> {

	UserRole findByName(String authority);
	
}
