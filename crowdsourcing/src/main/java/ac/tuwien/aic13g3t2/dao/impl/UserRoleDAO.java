package ac.tuwien.aic13g3t2.dao.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ac.tuwien.aic13g3t2.dao.IUserRoleDAO;
import ac.tuwien.aic13g3t2.entity.UserRole;

@Repository
public class UserRoleDAO extends HibernateDAO<UserRole, Long> implements IUserRoleDAO {

	@Override
	public UserRole findByName(String authority) {
		List<UserRole> authorities = currentSession().createCriteria(UserRole.class)
				.add(Restrictions.eq("authority", authority)).list();
		if (authorities.isEmpty())
				return null;
		return authorities.get(0);
	}

}
