package ac.tuwien.aic13g3t2.dao;

import java.util.List;

import ac.tuwien.aic13g3t2.entity.Task;

public interface ITaskDAO extends IGenericDAO<Task, Long> {

	List<Task> listUndone();

	List<Task> listDoneFromUser(Long userID);

	List<Task> listUndoneNotObsolete();

	List<Task> listUndoneFromUser(Long userID, int page);

	Double totalRewardFromUser(Long userID);

}
