package ac.tuwien.aic13g3t2.dao.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ac.tuwien.aic13g3t2.dao.IUserDAO;
import ac.tuwien.aic13g3t2.entity.User;

@Repository
public class UserDAO extends HibernateDAO<User, Long> implements IUserDAO {
	
	@Override
	public List<User> listBlocked(){
		return currentSession().createCriteria(User.class).add(Restrictions.eq("active", false)).list();
	}
	
	@Override
	public User findByName(String username) {
		List<User> users = currentSession().createCriteria(User.class)
				.add(Restrictions.eq("username", username)).list();
		if (users.isEmpty())
			return null;
		return users.get(0);
	}
	
}
