<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:include page="layout_top.jsp">
	<jsp:param name="title" value="Mentions" />
	<jsp:param name="menuActive" value="mentions" />
</jsp:include>

<h1>Mentions</h1>

<form method="POST" action="<c:url value="/mentions/detect" />" role="form" class="pull-right">
	<input type="submit" value="&raquo; Search for new mentions" class="btn btn-primary btn-lg" />
</form>

<p class="lead">
	There are currently <c:out value="${fn:length(mentions)}" /> mentions.
</p>

<p>
	Select a keywords from the <a href="<c:url value="/keywords" />">list of keywords</a> or
	an article from the <a href="<c:url value="/articles" />">list of articles</a> for details
	on a specific mention.
</p>

<jsp:include page="layout_bottom.jsp" />