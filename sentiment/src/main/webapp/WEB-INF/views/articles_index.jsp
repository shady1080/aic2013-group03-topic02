<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:include page="layout_top.jsp">
	<jsp:param name="title" value="Articles" />
	<jsp:param name="menuActive" value="articles" />
</jsp:include>

<h1>Articles</h1>

<form method="POST" action="<c:url value="/articles/extract" />" role="form" class="pull-right">
	<input type="submit" value="&raquo; Import Articles from Yahoo Finance" class="btn btn-primary btn-lg" />
</form>

<p class="lead">
	There are currently <c:out value="${fn:length(articles)}" /> articles.
</p>

<c:forEach var="article" items="${articles}" >
	<h2>
		<c:out value="${article.title}"/>
		<small>
			<c:out value="${article.publicationDate}"/>
			<small>
				<a href="<c:url value="/articles/${article.id}" />" class="btn btn-default btn-xs">&raquo; show mentions</a>
			</small>
		</small>
	</h2>
	<blockquote>
		<p>
			<c:out value="${article.text}"/>
		</p>
		<small>
			Source: <cite title="<c:out value="${article.url}" />"><a href="<c:out value="${article.url}" />" target="_blank"><c:out value="${article.url}" /></a></cite>
		</small>
	</blockquote>
	<hr />
</c:forEach>

<jsp:include page="layout_bottom.jsp" />