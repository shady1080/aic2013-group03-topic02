<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="layout_top.jsp">
	<jsp:param name="title" value="Home" />
	<jsp:param name="menuActive" value="home" />
</jsp:include>

<div class="jumbotron">
	<h1>Know your Sentiment.</h1>
	<p>
		Based on a list of <a href="<c:url value="/keywords" />">keywords</a>
		we scan <a href="<c:url value="/articles" />">news articles</a> for
		<a href="<c:url value="/mentions" />">mentions</a> of these keywords.
	</p>
	<p>
		A crowd of <a href="<c:url value="/baduser" />">workers</a> rates each
		mention and provides a basis for <a href="<c:url value="/sentiments" />">statistical reports</a>
		on the current and historical sentiment of each keyword.  
	</p>
	<p>
		<a class="btn btn-lg btn-primary" href="<c:url value="/keywords" />" role="button">Show the keywords �</a>
	</p>
</div>

<jsp:include page="layout_bottom.jsp" />