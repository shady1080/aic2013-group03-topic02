<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/style.css" />" />
	<title><c:out value="${param.title}" /> - Sentiment</title>
	
	<!-- Bootstrap core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>

 <div class="container">

	<!-- Static navbar -->
	<div class="navbar navbar-default" role="navigation">
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
	    <a class="navbar-brand" href="<c:url value="/" />">Sentiment</a>
	  </div>
	  <div class="navbar-collapse collapse">
	    <ul class="nav navbar-nav">
	      <li<c:if test="${param.menuActive eq \"home\"}"> class="active"</c:if>><a href="<c:url value="/" />">Home</a></li>
	      <li<c:if test="${param.menuActive eq \"keywords\"}"> class="active"</c:if>><a href="<c:url value="/keywords" />">Keywords</a></li>
	      <li<c:if test="${param.menuActive eq \"articles\"}"> class="active"</c:if>><a href="<c:url value="/articles" />">Articles</a></li>
	      <li<c:if test="${param.menuActive eq \"mentions\"}"> class="active"</c:if>><a href="<c:url value="/mentions" />">Mentions</a></li>
	      <li<c:if test="${param.menuActive eq \"workers\"}"> class="active"</c:if>><a href="<c:url value="/workers" />">Workers</a></li>
	      <li<c:if test="${param.menuActive eq \"tasks\"}"> class="active"</c:if>><a href="<c:url value="/tasks" />">Tasks</a></li>
	      <!-- <li<c:if test="${param.menuActive eq \"sentiments\"}"> class="active"</c:if>><a href="<c:url value="/sentiments" />">Sentiments</a></li>-->
	    </ul>
	  </div><!--/.nav-collapse -->
	</div>