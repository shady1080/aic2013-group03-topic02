<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<title>Sentiment statistics</title>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
	// Load the Visualization API and the piechart package.
	google.load('visualization', '1.0', {
		'packages' : [ 'corechart' ]
	});

	// Set a callback to run when the Google Visualization API is loaded.
	google.setOnLoadCallback(drawChart);

	//Parse data from server into JS arrays
	var datesArray = new Array();
	var valuesArray = new Array();
	<c:forEach var="date" items="${ratingsDate}">
		datesArray.push("<c:out value="${date}" />");
	</c:forEach>
	<c:forEach var="value" items="${ratingsValue}">
		valuesArray.push(<c:out value="${value}" />);
	</c:forEach>
	
	// Callback that creates and populates a data table,
	// instantiates the pie chart, passes in the data and
	// draws it.
	function drawChart() {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Date');
		data.addColumn('number', 'Rating');
		for(var i = 0; i < datesArray.length; i++){
			data.addRows([ [ datesArray[i], valuesArray[i]]]);	
		}
		// Set chart options
	var options = {
			'title' : 'Daily evolution of ratings',
			height:500,
			vAxis : {
				ticks: [{v:-1, f:"very bad"}, {v:0, f:"neutral"},{v:1, f:"very good"}],
				gridlines : {
					count : 5,
					color : 'black',
				}
			},
			hAxis : {
				textposition: 'out',
			},
			chartArea: {
				height: '30%'}
		};
		

		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.LineChart(document
				.getElementById('chart_div1'));
		chart.draw(data, options);
	}
</script>
<script>
	var datesArray = new Array();
	var valuesArray = new Array();
</script>
	<c:forEach var="date" items="${ratingsDate}">
		<script>datesArray.push("<c:out value="${date}" />".substring(0,10));</script>
	</c:forEach>
	
	<c:forEach var="value" items="${ratingsValue}">
		<script>
		var sub = 
		valuesArray.push(<c:out value="${value}" />);</script>
	</c:forEach>

</head>
<body>

	<jsp:include page="layout_top.jsp">
		<jsp:param name="title" value="Home" />
		<jsp:param name="menuActive" value="home" />
	</jsp:include>
	<h1>Sentiment</h1>
	<p class="lead">
		Sentiment statistic for <c:out value="${brand}" />
	</p>
	<c:if test="${empty overall_rating}">
		<h2>No overall rating found.</h2>
	</c:if>
	<p>
		Overall Result: 
		<c:out value="${overall_rating}" />
	</p>
	<div id="chart_div1"></div>
	<jsp:include page="layout_bottom.jsp" />
</body>
</html>
