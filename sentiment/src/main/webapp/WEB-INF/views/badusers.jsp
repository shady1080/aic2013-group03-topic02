<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:include page="layout_top.jsp">
	<jsp:param name="title" value="Workers" />
	<jsp:param name="menuActive" value="workers" />
</jsp:include>

<h1>Blocked Users</h1>
These users are blocked at the Crowsourcing Server.

<form method="POST" onsubmit="window.location.reload();" action="<c:url value="/workers" />" role="form" class="pull-right" >
	<input type="submit" value="&raquo; Locate workers with bad rating behavior" class="btn btn-primary btn-lg" />
</form>

<c:if test="${empty badusers}">
    <h2>No user is blocked.</h2>
</c:if>

<c:forEach var="userDA" items="${badusers}" >
<div class="userDA">
	<h2>User (#<c:out value="${userDA.id}" />) <c:out value="${userDA.username}" /></h2>
	<p class="clear"></p>
</div>
</c:forEach>

<jsp:include page="layout_bottom.jsp" />
