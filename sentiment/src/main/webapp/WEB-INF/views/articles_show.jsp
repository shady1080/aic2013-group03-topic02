<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:include page="layout_top.jsp">
	<jsp:param name="title" value="Articles" />
	<jsp:param name="menuActive" value="articles" />
</jsp:include>

<h1>Article Details</h1>

<h2>
	<c:out value="${article.title}"/>
	<small>
		<c:out value="${article.publicationDate}"/>
	</small>
</h2>
<blockquote>
	<p>
		<c:out value="${article.text}"/>
	</p>
	<small>
		Source: <cite title="<c:out value="${article.url}" />"><a href="<c:out value="${article.url}" />" target="_blank"><c:out value="${article.url}" /></a></cite>
	</small>
</blockquote>

<p class="lead">
	The article mentions <c:out value="${fn:length(mentions)}" /> keyword(s).
</p>

<c:forEach var="mention" items="${mentions}" >
	 <h2 style="display: block; float: left; margin: 15px;">
	 	<span class="label label-default" style="display: block;">
			<c:out value="${mention.keyword.keyword}"/><br/>
	 		<small style="color: gray;">(ID <c:out value="${mention.keyword.keywordID}"/>)</small>
	 	</span>
	 </h2>
</c:forEach>

<div style="clear: left;"></div>

<jsp:include page="layout_bottom.jsp" />