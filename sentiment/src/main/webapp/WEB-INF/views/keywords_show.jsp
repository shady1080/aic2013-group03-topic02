<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:include page="layout_top.jsp">
	<jsp:param name="title" value="Keyword: ${keyword.keyword}" />
	<jsp:param name="menuActive" value="keywords" />
</jsp:include>

<h1>Keyword: ${keyword.keyword}</h1>
<p class="lead">
	There are <c:out value="${fn:length(mentions)}" /> mentions of this keyword.
</p>

<c:forEach var="mention" items="${mentions}" >
	<h2>
		<c:out value="${mention.article.title}"/>
		<small>
			<c:out value="${mention.article.publicationDate}"/>
		</small>
	</h2>
	<blockquote>
		<p>
			<c:out value="${mention.article.text}"/>
		</p>
		<small>
			Source: <cite title="<c:out value="${mention.article.url}" />"><a href="<c:out value="${mention.article.url}" />" target="_blank"><c:out value="${mention.article.url}" /></a></cite>
		</small>
	</blockquote>
	<hr />
</c:forEach>

<jsp:include page="layout_bottom.jsp" />