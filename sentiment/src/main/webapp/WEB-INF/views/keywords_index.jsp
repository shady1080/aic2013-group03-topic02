<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:include page="layout_top.jsp">
	<jsp:param name="title" value="Keywords" />
	<jsp:param name="menuActive" value="keywords" />
</jsp:include>

<h1>Keywords</h1>

<p class="lead">There are currently <c:out value="${fn:length(keywords)}" /> keywords.</p>

<c:forEach var="keyword" items="${keywords}" >
  <h2 style="display: block; float: left; margin: 15px;">
  	<span class="label label-default" style="display: block;">
		<c:out value="${keyword.keyword}"/><br/>
		<small><a href="<c:url value="/keywords/${keyword.keywordID}" />"  style="color: rgb(197, 197, 197);">&raquo; mentions</a></small>
  		<small style="color: gray;">(ID <c:out value="${keyword.keywordID}"/>)</small></br>
  		<small><a href="<c:url value="/mentions/statistics/${keyword.keyword}" />" style="color: rgb(197, 197, 197);">&raquo; statistics</a></small>
  	</span>
  </h2>
</c:forEach>

<form method="POST" action="<c:url value="/keywords" />" class="form-inline" role="form" style="float: left; margin-top: 20px;">
	<div class="form-group">
		<input name="keyword" type="text" value="" placeholder="New Keyword" class="form-control input-lg" />
	</div>
	<input type="submit" value="Add Keyword" class="btn btn-primary btn-lg" />
</form>

<div style="clear: left;"></div>

<jsp:include page="layout_bottom.jsp" />