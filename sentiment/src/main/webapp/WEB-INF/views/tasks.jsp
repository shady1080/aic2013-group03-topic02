<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="layout_top.jsp">
	<jsp:param name="title" value="Tasks" />
	<jsp:param name="menuActive" value="tasks" />
</jsp:include>

<h1>Tasks</h1>
<p class="lead">There are currently <c:out value="${undoneTasks}" /> undone tasks in the crowd.</p>
<p>Repricing tasks will do the following:</p>
<form method="POST" action="<c:url value="/articles/reprice" />" role="form">
<ul>
	<li>
		Raise the reward by 25% of all undone tasks, that are older than
		<input size="3" name="maxHours" value="${maxHours}" /> hours and won't exceed a maximum reward of
		<input size="5" name="maxReward" value="${maxReward}" /> cents.
	</li>
	<li>Remove those tasks from the crowd, where the reward exceeds the threshold.</li>
	<li>Submit new tasks for ratings that have no corresponding tasks in the crowd yet.</li>
	<li>Re-Submit obsolete tasks if the reward threshold has been changed.</li>
</ul>
	<input type="submit" value="&raquo; Reprice Tasks" class="btn btn-primary" />
</form>

<jsp:include page="layout_bottom.jsp" />