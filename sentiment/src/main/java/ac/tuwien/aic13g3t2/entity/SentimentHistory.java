package ac.tuwien.aic13g3t2.entity;

import java.util.Date;

public class SentimentHistory {
	private Double sentimentRating;
	
	private Date date;
	
	public SentimentHistory() {
		
	}

	public Double getSentimentRating() {
		return sentimentRating;
	}

	public void setSentimentRating(Double sentimentRating) {
		this.sentimentRating = sentimentRating;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "SentimentHistory [sentimentRating=" + sentimentRating
				+ ", date=" + date + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result
				+ ((sentimentRating == null) ? 0 : sentimentRating.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SentimentHistory other = (SentimentHistory) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (sentimentRating == null) {
			if (other.sentimentRating != null)
				return false;
		} else if (!sentimentRating.equals(other.sentimentRating))
			return false;
		return true;
	}
}
