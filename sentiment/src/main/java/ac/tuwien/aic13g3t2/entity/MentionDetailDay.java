package ac.tuwien.aic13g3t2.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MentionDetailDay implements Comparable<MentionDetailDay>{
	
	private List<Mention> mentionList;
	
	private double mean;
	
	private double sampleVariance;
	
	private double standardDeviation;
	
	private double t_i;
	
	private Date date;
	
	public MentionDetailDay() {
		
	}

	public List<Mention> getMentionList() {
		return mentionList;
	}

	public void setMentionList(List<Mention> mentionList) {
		this.mentionList = mentionList;
	}

	public double getMean() {
		return mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public double getSampleVariance() {
		return sampleVariance;
	}

	public void setSampleVariance(double sampleVariance) {
		this.sampleVariance = sampleVariance;
	}

	public double getStandardDeviation() {
		return standardDeviation;
	}

	public void setStandardDeviation(double standardDeviation) {
		this.standardDeviation = standardDeviation;
	}

	public double getT_i() {
		return t_i;
	}

	public void setT_i(double t_i) {
		this.t_i = t_i;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	/*
	 * Returns a list of all ratings of all mentions
	 */
	public List<Rating> getAllRatingsForMentions() {
		List<Rating> ratings = null;
		
		if (mentionList.size() > 0 && mentionList != null) {
			ratings = new ArrayList<Rating>();
			
			for(Mention m: mentionList) {
				ratings.addAll(m.getRatings());
			}
			
		}
		
		return ratings;
	}
	
	@Override
	// returns a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object. 
	public int compareTo(MentionDetailDay o) {
		if (o.getStandardDeviation() == this.getStandardDeviation()) {
			return 0;
		}
		else if (o.getStandardDeviation() > this.getStandardDeviation()) {
			return -1;
		}
		else {
			return 1;
		}
	}

	@Override
	public String toString() {
		return "MentionDetailDay [mentionList=" + mentionList + ", mean="
				+ mean + ", sampleVariance=" + sampleVariance
				+ ", standardDeviation=" + standardDeviation + ", t_i=" + t_i
				+ ", date=" + date + "]";
	}
	
}
