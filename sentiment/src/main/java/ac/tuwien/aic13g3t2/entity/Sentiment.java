package ac.tuwien.aic13g3t2.entity;

import java.util.List;

public class Sentiment {

	private Double totalSentiment;
	
	private List<SentimentRating> sentimentRating;
	
	private List<SentimentHistory> sentimentHistory;
	
	public Sentiment () {
		
	}

	public Double getTotalSentiment() {
		return totalSentiment;
	}

	public void setTotalSentiment(Double totalSentiment) {
		this.totalSentiment = totalSentiment;
	}

	public List<SentimentRating> getSentimentRating() {
		return sentimentRating;
	}

	public void setSentimentRating(List<SentimentRating> sentimentRating) {
		this.sentimentRating = sentimentRating;
	}
	
	public List<SentimentHistory> getSentimentHistory() {
		return sentimentHistory;
	}

	public void setSentimentHistory(List<SentimentHistory> sentimentHistory) {
		this.sentimentHistory = sentimentHistory;
	}

	@Override
	public String toString() {
		return "Sentiment [totalSentiment=" + totalSentiment
				+ ", sentimentRating=" + sentimentRating
				+ ", sentimentHistory=" + sentimentHistory + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((sentimentHistory == null) ? 0 : sentimentHistory.hashCode());
		result = prime * result
				+ ((sentimentRating == null) ? 0 : sentimentRating.hashCode());
		result = prime * result
				+ ((totalSentiment == null) ? 0 : totalSentiment.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sentiment other = (Sentiment) obj;
		if (sentimentHistory == null) {
			if (other.sentimentHistory != null)
				return false;
		} else if (!sentimentHistory.equals(other.sentimentHistory))
			return false;
		if (sentimentRating == null) {
			if (other.sentimentRating != null)
				return false;
		} else if (!sentimentRating.equals(other.sentimentRating))
			return false;
		if (totalSentiment == null) {
			if (other.totalSentiment != null)
				return false;
		} else if (!totalSentiment.equals(other.totalSentiment))
			return false;
		return true;
	}
}
