package ac.tuwien.aic13g3t2.entity;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ac.tuwien.aic13g3t2.util.Http;
import ac.tuwien.aic13g3t2.util.Settings;

@Entity
@Table(name = "rating")
public class Rating {
	
	public static final int TARGET_RATING_COUNT = 3;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ratingID;
	
	@ManyToOne
	private Mention mention;
	
	private String rating;
	private Integer userID;
	private final Long creationDate = System.currentTimeMillis();
	
	public Rating() {
	}

	public Long getRatingID() {
		return ratingID;
	}

	public void setRatingID(Long ratingID) {
		this.ratingID = ratingID;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public Mention getMention() {
		return mention;
	}

	public void setMention(Mention mention) {
		this.mention = mention;
	}

	@Override
	public String toString() {
		return "Rating [ratingID=" + ratingID + ", rating=" + rating
				+ ", userID=" + userID + ", mention=" + mention + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mention == null) ? 0 : mention.hashCode());
		result = prime * result + ((rating == null) ? 0 : rating.hashCode());
		result = prime * result
				+ ((ratingID == null) ? 0 : ratingID.hashCode());
		result = prime * result + ((userID == null) ? 0 : userID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rating other = (Rating) obj;
		if (mention == null) {
			if (other.mention != null)
				return false;
		} else if (!mention.equals(other.mention))
			return false;
		if (rating == null) {
			if (other.rating != null)
				return false;
		} else if (!rating.equals(other.rating))
			return false;
		if (ratingID == null) {
			if (other.ratingID != null)
				return false;
		} else if (!ratingID.equals(other.ratingID))
			return false;
		if (userID == null) {
			if (other.userID != null)
				return false;
		} else if (!userID.equals(other.userID))
			return false;
		return true;
	}

	public void publish() {
		Map<String,String> data = new HashMap<String,String>();
		data.put("ratingID", ratingID + "");
		data.put("taskDescription", "Please rate '" + mention.getKeyword().getKeyword() + "' in the following article. "
				+ mention.getArticle().getTitle() + ". " + mention.getArticle().getText());
		data.put("answerPossibilities", "very good, good, neutral, bad, very bad");
		data.put("price", "1");
		data.put("callbackURL", Settings.SENTIMENTURL + "/sentiment/" + ratingID);
		
		try { /* send POST request */
			Http.post(Settings.CROWDSOURCEURL + "/tasks", data);
		} catch (IOException e) {
			// ignore
		}
	}

	public Long getCreationDate() {
		return creationDate;
	}
	
}
