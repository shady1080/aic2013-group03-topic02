package ac.tuwien.aic13g3t2.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "mention")
public class Mention {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long mentionID;
	
	@ManyToOne
	private Keyword keyword;
	
	@ManyToOne
	private Article article;
	
	@OneToMany(mappedBy="mention")
	private List<Rating> ratings;
	
	public Mention() {
		this.ratings = new ArrayList<Rating>();
	}

	public Long getMentionID() {
		return mentionID;
	}

	public void setMentionID(Long mentionID) {
		this.mentionID = mentionID;
	}

	public Keyword getKeyword() {
		return keyword;
	}

	public void setKeyword(Keyword keyword) {
		this.keyword = keyword;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public List<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	@Override
	public String toString() {
		return "Mention [mentionID=" + mentionID + ",\n\tkeyword=" + keyword
				+ ",\n\t\tarticle=" + article + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((article == null) ? 0 : article.hashCode());
		result = prime * result
				+ ((keyword == null) ? 0 : keyword.hashCode());
		result = prime * result
				+ ((mentionID == null) ? 0 : mentionID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mention other = (Mention) obj;
		if (article == null) {
			if (other.article != null)
				return false;
		} else if (!article.equals(other.article))
			return false;
		if (keyword == null) {
			if (other.keyword != null)
				return false;
		} else if (!keyword.equals(other.keyword))
			return false;
		if (mentionID == null) {
			if (other.mentionID != null)
				return false;
		} else if (!mentionID.equals(other.mentionID))
			return false;
		return true;
	}

	

}
