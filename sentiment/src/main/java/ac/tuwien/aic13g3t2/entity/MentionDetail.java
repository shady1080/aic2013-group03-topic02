package ac.tuwien.aic13g3t2.entity;

public class MentionDetail implements Comparable<MentionDetail> {
	
	private Mention mention;
	
	private double mean;
	
	private double sampleVariance;
	
	private double standardDeviation;
	
	private double t_i;

	public Mention getMention() {
		return mention;
	}

	public void setMention(Mention mention) {
		this.mention = mention;
	}

	public double getMean() {
		return mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public double getSampleVariance() {
		return sampleVariance;
	}

	public void setSampleVariance(double sampleVariance) {
		this.sampleVariance = sampleVariance;
	}

	public double getStandardDeviation() {
		return standardDeviation;
	}

	public void setStandardDeviation(double standardDeviation) {
		this.standardDeviation = standardDeviation;
	}

	public double getT_i() {
		return t_i;
	}

	public void setT_i(double t_i) {
		this.t_i = t_i;
	}

	@Override
	// returns a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object. 
	public int compareTo(MentionDetail o) {
		if (o.getStandardDeviation() == this.getStandardDeviation()) {
			return 0;
		}
		else if (o.getStandardDeviation() > this.getStandardDeviation()) {
			return -1;
		}
		else {
			return 1;
		}
	}

	@Override
	public String toString() {
		return "MentionDetail [mention=" + mention + ", mean=" + mean
				+ ", sampleVariance=" + sampleVariance + ", standardDeviation="
				+ standardDeviation + ", t_i=" + t_i + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(mean);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((mention == null) ? 0 : mention.hashCode());
		temp = Double.doubleToLongBits(sampleVariance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(standardDeviation);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(t_i);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MentionDetail other = (MentionDetail) obj;
		if (Double.doubleToLongBits(mean) != Double
				.doubleToLongBits(other.mean))
			return false;
		if (mention == null) {
			if (other.mention != null)
				return false;
		} else if (!mention.equals(other.mention))
			return false;
		if (Double.doubleToLongBits(sampleVariance) != Double
				.doubleToLongBits(other.sampleVariance))
			return false;
		if (Double.doubleToLongBits(standardDeviation) != Double
				.doubleToLongBits(other.standardDeviation))
			return false;
		if (Double.doubleToLongBits(t_i) != Double.doubleToLongBits(other.t_i))
			return false;
		return true;
	}

}
