package ac.tuwien.aic13g3t2.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "keyword")
public class Keyword {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long keywordID;
	
	private String keyword;
	
	public Keyword() {
	}

	public Long getKeywordID() {
		return keywordID;
	}

	public void setKeywordID(Long keywordID) {
		this.keywordID = keywordID;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	@Override
	public String toString() {
		return "Keyword [keywordID=" + keywordID + ", keyword=" + keyword + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
		result = prime * result
				+ ((keywordID == null) ? 0 : keywordID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Keyword other = (Keyword) obj;
		if (keyword == null) {
			if (other.keyword != null)
				return false;
		} else if (!keyword.equals(other.keyword))
			return false;
		if (keywordID == null) {
			if (other.keywordID != null)
				return false;
		} else if (!keywordID.equals(other.keywordID))
			return false;
		return true;
	}
}
