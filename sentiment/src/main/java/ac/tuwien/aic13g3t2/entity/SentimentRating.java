package ac.tuwien.aic13g3t2.entity;

import java.util.Date;

public class SentimentRating {
	
	private Double sentimentRating;
	
	private Date date;
	
	private String articleURL;
	
	public SentimentRating() {
		
	}

	public Double getSentimentRating() {
		return sentimentRating;
	}

	public void setSentimentRating(Double sentimentRating) {
		this.sentimentRating = sentimentRating;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getArticleURL() {
		return articleURL;
	}

	public void setArticleURL(String articleURL) {
		this.articleURL = articleURL;
	}

	@Override
	public String toString() {
		return "SentimentRating [SentimentRating=" + sentimentRating
				+ ", date=" + date + ", articleURL=" + articleURL + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((sentimentRating == null) ? 0 : sentimentRating.hashCode());
		result = prime * result
				+ ((articleURL == null) ? 0 : articleURL.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SentimentRating other = (SentimentRating) obj;
		if (sentimentRating == null) {
			if (other.sentimentRating != null)
				return false;
		} else if (!sentimentRating.equals(other.sentimentRating))
			return false;
		if (articleURL == null) {
			if (other.articleURL != null)
				return false;
		} else if (!articleURL.equals(other.articleURL))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		return true;
	}

}
