package ac.tuwien.aic13g3t2.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public final class Settings {
	//public static String CROWDSOURCEURL = "http://5.175.172.128:8080/crowdsourcing";
	public static String CROWDSOURCEURL = "http://localhost:8080/aic13g3t2-crowdsourcing";
	
	//public static String SENTIMENTURL = "http://84.200.10.39:8080/sentiment";
	public static String SENTIMENTURL = "http://localhost:8080/aic13g3t2-sentiment";

	private static Properties properties = new Properties();
	private static String settingsFilePath = System.getProperty("user.home") + "/.aic13";
	private static String settingsFileName = "settings.xml";
	private static String settingsFullPath = settingsFilePath + "/" + settingsFileName;
	
	public enum SettingsKeys {
		MAXHOURS,
		MAXREWARD
	}
	
	public static void store(SettingsKeys key, String value)
			throws InvalidPropertiesFormatException, FileNotFoundException, IOException {
		checkIfLoaded();
		properties.setProperty(key.name(), value);
		properties.storeToXML(new FileOutputStream(new File(settingsFullPath)), "");
	}
	
	private static void checkIfLoaded() throws IOException {
		// check if path exists
		File path = new File(settingsFilePath);
		if (!path.exists()) {
			path.mkdir();
		}
		
		// check if settings.xml exists
		File file = new File(settingsFullPath);
		if (!file.exists()) {
			file.createNewFile();
			properties.storeToXML(new FileOutputStream(file), "");
		} else {
			properties.loadFromXML(new FileInputStream(file));
		}
	}

	public static String get(SettingsKeys key)
			throws InvalidPropertiesFormatException, FileNotFoundException, IOException {
		checkIfLoaded();
		return properties.getProperty(key.name());
	}
	
}
