package ac.tuwien.aic13g3t2.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Http {

	private static final Logger log = LoggerFactory.getLogger(Http.class);
	//private static final ObjectMapper mapper = new ObjectMapper();
	
	/*private static String perform(String requestMethod, String path, String json) throws IOException {
		System.out.println("PATH: " + path);
		System.out.println("DEBUG: " + json);
		
		URL url = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(requestMethod);
		conn.setRequestProperty("Content-Type", "application/json");
		
		if (json != null && !json.isEmpty()) { // construct json parameter string from data
			OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
			out.write(json);
			out.close();
		}
		
		/* receive response *
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = null;
        String response = "";
        while ((line = rd.readLine()) != null) {
        	response += line;
        }
        rd.close();
        
        return response;
		
	}
	
	public static String put(String path, Task t) throws IOException {
		return perform("PUT", path, mapper.writeValueAsString(t));
	}
	
	public static String get(String path) throws IOException {
		return perform("GET", path, null);
	}

	public static String post(String path, Task t) throws IOException {
		return perform("POST", path, mapper.writeValueAsString(t));
	}

	public static String delete(String path, Task t) throws IOException {
		return perform("DELETE", path, mapper.writeValueAsString(t));
	}

	public static String post(String path) throws IOException {
		return perform("POST", path, null);
	}

	public static String post(String path, Map<String, String> data) throws IOException {
		return perform("POST", path, mapper.writeValueAsString(data));
	}*/
	
	public static String post(String path) throws IOException {
		return perform("POST", path, null);
	}
	
	public static String post(String path, Map<String, String> data) throws IOException {
		return perform("POST", path, toString(data));
	}
	
	public static String get(String path) throws IOException {
		return perform("GET", path, null);
	}
	
	public static String put(String path, String s) throws IOException {
		return perform("PUT", path, s);
	}
	
	public static String delete(String path) throws IOException {
		return perform("DELETE", path, null);
	}
	
	private static String toString(Map<String,String> map) {
		String s = "";
		for (Entry<String,String> e : map.entrySet())
			s += e.getKey() + "=" + e.getValue() + "&";
		return s;
	}
	
	private static String perform(String method, String path, String data) throws IOException {
		
        /* send request */
        URL url = new URL(path);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod(method);
        if (data != null && !data.isEmpty()) {
	        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
	        wr.write(data);
	        wr.flush();
	        wr.close();
		}
		
        /* receive response */
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = null;
        String response = "";
        while ((line = rd.readLine()) != null) {
        	response += line;
        }
        rd.close();
        
        return response;
	}
	
}
