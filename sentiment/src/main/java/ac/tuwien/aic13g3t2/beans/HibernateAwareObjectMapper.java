package ac.tuwien.aic13g3t2.beans;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;


public class HibernateAwareObjectMapper extends ObjectMapper {
  
    /**
	 * 
	 */
	private static final long serialVersionUID = -5511501310980040258L;

	/**
     * Register module to support lazy loaded items
     */
	public HibernateAwareObjectMapper() {
        registerModule(new Hibernate4Module());
    }

}
