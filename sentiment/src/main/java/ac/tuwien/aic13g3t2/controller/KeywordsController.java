package ac.tuwien.aic13g3t2.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ac.tuwien.aic13g3t2.entity.Keyword;
import ac.tuwien.aic13g3t2.entity.Mention;
import ac.tuwien.aic13g3t2.service.IKeywordsService;
import ac.tuwien.aic13g3t2.service.IMentionService;

@Controller
@RequestMapping("keywords")
public class KeywordsController {
	@Autowired
	private IKeywordsService keywordsService;
	
	@Autowired
	private IMentionService mentionsService;
	
	private static final Logger logger = LoggerFactory.getLogger(KeywordsController.class);
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Locale locale, Model model) throws IOException {
		model.addAttribute("keywords", keywordsService.list());
		return "keywords_index";
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String create(Locale locale, Model model, @RequestParam(value="keyword") String name) throws IOException {
		
		logger.debug(name);
		
		if(name.length() > 0 && keywordsService.find(name) == null) {

			Keyword keyword = new Keyword();
			keyword.setKeyword(name);
			
			keywordsService.add(keyword);
			
			logger.info("Added new keyword: \"" + keyword + "\".");
			
		}
		
		return "redirect:keywords";
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public String show(Locale locale, Model model, @PathVariable Long id) throws IOException {		
		Keyword keyword = keywordsService.find(id);
		List<Mention> mentions = mentionsService.findByKeyword(keyword);

		model.addAttribute("keyword", keyword);
		model.addAttribute("mentions", mentions);
		
		return "keywords_show";
	}
	
	@RequestMapping(value = "readkeywords", method = RequestMethod.GET)
	@ResponseBody
	public void readkeywords(Locale locale, Model model) throws IOException {		
		keywordsService.readKeywords();	
	}
	
	
}
