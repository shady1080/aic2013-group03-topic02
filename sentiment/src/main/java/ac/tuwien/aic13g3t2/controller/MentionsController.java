package ac.tuwien.aic13g3t2.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ac.tuwien.aic13g3t2.entity.Sentiment;
import ac.tuwien.aic13g3t2.entity.SentimentHistory;
import ac.tuwien.aic13g3t2.entity.SentimentRating;
import ac.tuwien.aic13g3t2.service.IMentionService;

@Controller
@RequestMapping("mentions")
public class MentionsController {
	
	private static final Logger logger = LoggerFactory.getLogger(MentionsController.class);

	@Autowired
	private IMentionService mentionService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Locale locale, Model model) throws IOException {
		model.addAttribute("mentions", mentionService.list());
		return "mentions_index";
	}
	
	@RequestMapping(value = "detect", method = RequestMethod.POST)
	public String detectMentions(Locale locale, Model model) throws IOException {
		
		mentionService.detect();
		
		return "redirect:";
	}
	
	@RequestMapping(value = "sentiment/{keyword}", method = RequestMethod.GET)
	@ResponseBody
	public Sentiment getMentions(Locale locale, Model model, @PathVariable String keyword) throws IOException {
		
		logger.info("Starting evaluation of keyword: " + keyword);
		
		return mentionService.evaluate(keyword);
	}
	
	@RequestMapping(value = "statistics/{keyword}", method = RequestMethod.GET)
	public String getStatistics(Locale locale, Model model, @PathVariable String keyword) throws IOException {
		//TODO use real sentiments, this is for testing
		Sentiment sentiment = mentionService.evaluate(keyword);
		
		System.out.println("... called statistics for keyword " + keyword);
		System.out.println(sentiment.toString());
		
		/*
		Sentiment sentiment = new Sentiment();
		
		SentimentRating sentimentRating1 = new SentimentRating();
		SentimentRating sentimentRating2 = new SentimentRating();
		SentimentRating sentimentRating3 = new SentimentRating();
		SentimentRating sentimentRating4 = new SentimentRating();
		SentimentRating sentimentRating5 = new SentimentRating();
		SentimentRating sentimentRating6 = new SentimentRating();
		sentimentRating1.setDate(new Date());
		sentimentRating2.setDate(new Date());
		sentimentRating3.setDate(new Date());
		sentimentRating4.setDate(new Date());
		sentimentRating5.setDate(new Date());
		sentimentRating6.setDate(new Date());
		sentimentRating1.setSentimentRating(1.0);
		sentimentRating2.setSentimentRating(0.0);
		sentimentRating3.setSentimentRating(2.0);
		sentimentRating4.setSentimentRating(2.5);
		sentimentRating5.setSentimentRating(3.0);
		sentimentRating6.setSentimentRating(1.0);
		List<SentimentRating> sentimentRatings = new ArrayList<SentimentRating>();
		sentimentRatings.add(sentimentRating1);
		sentimentRatings.add(sentimentRating2);
		sentimentRatings.add(sentimentRating3);
		sentimentRatings.add(sentimentRating4);
		sentimentRatings.add(sentimentRating5);
		sentimentRatings.add(sentimentRating6);
		
		sentiment.setSentimentRating(sentimentRatings);
		sentiment.setTotalSentiment(3.0);
		*/
		
		//Real implementation starts here
		List<String> datesList = new ArrayList<String>();
		List<String> valuesList = new ArrayList<String>();
		// old code accessing the SentimentRatings
		/*
		if (sentiment.getSentimentRating() != null && sentiment.getSentimentRating().size() > 0) {
			for (SentimentRating sentimentRating : sentiment.getSentimentRating()) {
				datesList.add(sentimentRating.getDate().toString());
				valuesList.add(sentimentRating.getSentimentRating().toString());
			}
		}
		*/
		// the new code to access the SentimentHistory
		if (sentiment.getSentimentHistory() != null && sentiment.getSentimentHistory().size() > 0) {
			for (SentimentHistory sentimentHistory : sentiment.getSentimentHistory()) {
				datesList.add(sentimentHistory.getDate().toString());
				if (sentimentHistory.getSentimentRating() == null) {
					valuesList.add("null");
				}
				else {
					valuesList.add(sentimentHistory.getSentimentRating().toString());
				}
				
				
			}
		}
		
		model.addAttribute("brand", keyword);
		if (sentiment.getSentimentRating() != null && sentiment.getSentimentRating().size() > 0) {
			model.addAttribute("overall_rating", sentiment.getTotalSentiment());
		}
		else {
			model.addAttribute("overall_rating", "no ratings yet!!");
		}
		model.addAttribute("ratingsDate", datesList);
		model.addAttribute("ratingsValue", valuesList);
		return "statistics";
	}
	
	//Parse Java array into JS array to output on JSP
	private String getArrayStringForInt(String[] items){
	    String result = "[";
	    for(int i = 0; i < items.length; i++) {
	        //result += "\"" + items[i] + "\"";
	    	result += items[i];
	        if(i < items.length - 1) {
	            result += ", ";
	        }
	    }
	    result += "]";

	    return result;
	}

	private String getArrayStringForString(String[] items){
	    String result = "[";
	    for(int i = 0; i < items.length; i++) {
	        result += "'" + items[i] + "'";
	        if(i < items.length - 1) {
	            result += ", ";
	        }
	    }
	    result += "]";

	    return result;
	}
	
}
