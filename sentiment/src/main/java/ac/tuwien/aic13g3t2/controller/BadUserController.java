package ac.tuwien.aic13g3t2.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ac.tuwien.aic13g3t2.entity.UserDA;
import ac.tuwien.aic13g3t2.service.IBadUserService;

@Controller
@RequestMapping("workers")
public class BadUserController {
	@Autowired
	private IBadUserService badUserService;
	
	private static final Logger logger = LoggerFactory.getLogger(BadUserController.class);
	
	@RequestMapping(method = RequestMethod.POST)
	public String findBadUser(Locale locale, Model model) throws IOException {		
		badUserService.findBadUsers();	
		
		return "redirect:";
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String listBadUser(Locale locale, Model model) throws IOException {		
		List<UserDA> users = badUserService.getBlockedUsers();
		model.addAttribute("badusers", users);
		return "badusers";
	}
	
}
