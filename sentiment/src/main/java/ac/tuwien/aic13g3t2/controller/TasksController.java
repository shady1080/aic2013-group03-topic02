package ac.tuwien.aic13g3t2.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.Locale;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ac.tuwien.aic13g3t2.entity.Task;
import ac.tuwien.aic13g3t2.util.Http;
import ac.tuwien.aic13g3t2.util.Settings;
import ac.tuwien.aic13g3t2.util.Settings.SettingsKeys;

@Controller
@RequestMapping("tasks")
public class TasksController {
	
	private static final Logger logger = LoggerFactory.getLogger(TasksController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public String listTasks(Locale locale, Model model) throws IOException {		
		
		// get map of all undone tasks from crowdsourcing-server
		Integer numTasks = Integer.parseInt(Http.get(Settings.CROWDSOURCEURL + "/numundonetasks"));
		
		// load configured values
		Long maxHours = Long.parseLong(getConfiguredValue(SettingsKeys.MAXHOURS, "24"));
		Double maxReward = Double.parseDouble(getConfiguredValue(SettingsKeys.MAXREWARD, "3.0"));
		
		model.addAttribute("undoneTasks", numTasks);
		model.addAttribute("maxHours", maxHours);
		model.addAttribute("maxReward", maxReward);
		return "tasks";
	}

	private String getConfiguredValue(SettingsKeys key, String defaultValue)
			throws InvalidPropertiesFormatException, FileNotFoundException, IOException {
		if (Settings.get(key) != null)
			return Settings.get(key);
		else {
			Settings.store(key, defaultValue);
			return defaultValue;
		}
	}
	
}
