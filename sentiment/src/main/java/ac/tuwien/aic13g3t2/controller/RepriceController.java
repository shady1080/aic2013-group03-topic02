package ac.tuwien.aic13g3t2.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ac.tuwien.aic13g3t2.entity.Task;
import ac.tuwien.aic13g3t2.service.IMentionService;
import ac.tuwien.aic13g3t2.util.Http;
import ac.tuwien.aic13g3t2.util.Settings;

@Controller
public class RepriceController {

	private static final Logger logger = LoggerFactory.getLogger(RepriceController.class);

	@Autowired
	private IMentionService mentionService;
	
	@RequestMapping(value = "articles/reprice", method = RequestMethod.POST)
	public String repriceTasks(Locale locale, Model model,
			@RequestParam(value="maxHours") Long maxHours,
			@RequestParam(value="maxReward") Double maxReward) throws IOException {
		
		mentionService.reprice(maxHours, maxReward);
		
		// get map of all undone tasks from crowdsourcing-server
		Integer numTasks = Integer.parseInt(Http.get(Settings.CROWDSOURCEURL + "/numundonetasks"));
				
		// load configured values
		model.addAttribute("undoneTasks", numTasks);
		model.addAttribute("maxHours", maxHours);
		model.addAttribute("maxReward", maxReward);
		return "tasks";
	}
	
}
