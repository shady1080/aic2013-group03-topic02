package ac.tuwien.aic13g3t2.controller;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ac.tuwien.aic13g3t2.entity.Article;
import ac.tuwien.aic13g3t2.entity.Mention;
import ac.tuwien.aic13g3t2.service.IArticleService;
import ac.tuwien.aic13g3t2.service.IMentionService;

@Controller
@RequestMapping("articles")
public class ArticlesController {
	@Autowired
	private IArticleService articleService;
	
	@Autowired
	private IMentionService mentionsService;
	
	private static final Logger logger = LoggerFactory.getLogger(ArticlesController.class);
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(Locale locale, Model model) throws IOException {		
		model.addAttribute("articles", articleService.list());
		return "articles_index";
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public String show(Locale locale, Model model, @PathVariable Long id) throws IOException {		
		Article article = articleService.find(id);
		List<Mention> mentions = mentionsService.findByArticle(article);

		model.addAttribute("article", article);
		model.addAttribute("mentions", mentions);
		
		return "articles_show";
	}
	
	@RequestMapping(value = "extract", method = RequestMethod.POST)
	public String extractYahooFinanceRss(Locale locale, Model model) throws IOException {
		
		articleService.extract();
		
		return "redirect:";
	}
	
}
