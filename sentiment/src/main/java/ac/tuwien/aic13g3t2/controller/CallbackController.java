package ac.tuwien.aic13g3t2.controller;

import java.io.IOException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ac.tuwien.aic13g3t2.entity.Rating;
import ac.tuwien.aic13g3t2.service.ICallbackService;

@Controller
@RequestMapping("sentiment")
public class CallbackController {
	@Autowired
	private ICallbackService callbackService;
	
	private static final Logger logger = LoggerFactory.getLogger(CallbackController.class);
	
	@RequestMapping(value = "{ratingID}", method = RequestMethod.PUT)
	@ResponseBody
	public void callback(@PathVariable Long ratingID,  @RequestBody Rating rating,
			Locale locale, Model model) throws IOException {		
		callbackService.callback(ratingID, rating);	
	}
	
	
}
