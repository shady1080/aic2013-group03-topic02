package ac.tuwien.aic13g3t2.dao;

import java.util.List;

import ac.tuwien.aic13g3t2.entity.Article;
import ac.tuwien.aic13g3t2.entity.Keyword;
import ac.tuwien.aic13g3t2.entity.Mention;

public interface IMentionDAO extends IGenericDAO<Mention, Long> {

	boolean isExisting(Article article, Keyword keyword);
	
	List<Mention> listUnrated();

	List<Mention> findByKeyword(Keyword keyword);

	List<Mention> findByArticle(Article article);

}
