package ac.tuwien.aic13g3t2.dao.impl;

import java.io.File;

import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ac.tuwien.aic13g3t2.dao.IKeywordDAO;
import ac.tuwien.aic13g3t2.entity.Keyword;

@Repository
public class KeywordDAO extends HibernateDAO<Keyword, Long> implements IKeywordDAO {

	@Autowired
	ServletContext servletContext;
	
	@Override
	public void readKeywords() {
		try {

				File stocks = new File(servletContext.getRealPath("/WEB-INF/Keywords/keywords.xml"));
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(stocks);
				doc.getDocumentElement().normalize();
	
				NodeList nodes = doc.getElementsByTagName("keyword");
				for (int i = 0; i < nodes.getLength(); i++) {
					Node node = nodes.item(i);
		
					String tkeyword = node.getTextContent();
					
					if(find(tkeyword) == null){
						Keyword tkeyw = new Keyword();
						tkeyw.setKeyword(tkeyword);
						
						add(tkeyw);
						
						System.out.println("new keyword: "+ tkeyword);
					}
					
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			

	}

	@Override
	public Keyword find(String keyword) {
		return (Keyword)this.currentSession().createCriteria(Keyword.class).add(Restrictions.eq("keyword", keyword)).uniqueResult();
	}

}
