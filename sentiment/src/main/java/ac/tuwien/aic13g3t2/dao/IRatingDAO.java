package ac.tuwien.aic13g3t2.dao;

import java.util.List;

import ac.tuwien.aic13g3t2.entity.Rating;
import ac.tuwien.aic13g3t2.entity.SentimentRating;

public interface IRatingDAO extends IGenericDAO<Rating, Long> {
	
	List<SentimentRating> getAvgRatingsByKeyword(String keyword);
	
	public Double getOverallAvgRatingsByKeyword(String keyword);
	
	public List<Rating> getRatingsForUser(Integer userID);
	
	public List<Rating> getRatingsForMention(Long mentionID);
	
	public Integer getRatingCountForUser(Integer userID);
	
	public List<Integer> getUserListForRatings();

	List<Rating> getUnratedRatings(Long hours);

	
}
