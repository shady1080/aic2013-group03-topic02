package ac.tuwien.aic13g3t2.dao;

import ac.tuwien.aic13g3t2.entity.Keyword;

public interface IKeywordDAO extends IGenericDAO<Keyword, Long> {
	
	public void readKeywords();
	
	public Keyword find(String keyword);

}
