package ac.tuwien.aic13g3t2.dao.impl;

import java.util.List;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ac.tuwien.aic13g3t2.dao.IMentionDAO;
import ac.tuwien.aic13g3t2.entity.Article;
import ac.tuwien.aic13g3t2.entity.Keyword;
import ac.tuwien.aic13g3t2.entity.Mention;

@Repository
public class MentionDAO extends HibernateDAO<Mention, Long> implements IMentionDAO {
	
	

	@Override
	public boolean isExisting(Article article, Keyword keyword) {
		return ((Number) currentSession().createCriteria(Mention.class).add(Restrictions.eq("article", article)).add(Restrictions.eq("keyword", keyword)).setProjection(Projections.rowCount()).uniqueResult()).intValue() > 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Mention> listUnrated() { 
		//return currentSession().createQuery("from Mention as mention left join mention.ratings as ratings where count(ratings) < " + Rating.TARGET_RATING_COUNT).list();
		return currentSession().createQuery("from Mention").list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Mention> findByKeyword(Keyword keyword) {
		return currentSession().createQuery("from Mention where keyword.keywordID = :keywordID").setLong("keywordID", keyword.getKeywordID()).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Mention> findByArticle(Article article) {
		return currentSession().createQuery("from Mention where article.id = :articleID").setLong("articleID", article.getId()).list();
	}

}
