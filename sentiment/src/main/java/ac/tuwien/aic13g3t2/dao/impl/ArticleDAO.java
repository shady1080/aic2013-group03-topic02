package ac.tuwien.aic13g3t2.dao.impl;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ac.tuwien.aic13g3t2.dao.IArticleDAO;
import ac.tuwien.aic13g3t2.entity.Article;

@Repository
public class ArticleDAO extends HibernateDAO<Article, Long> implements IArticleDAO{
  
	public boolean isExisting(String title) {
        return ((Number) currentSession().createCriteria(Article.class).add(Restrictions.eq("title", title)).setProjection(Projections.rowCount()).uniqueResult()).intValue() > 0;
	}
	
}
