package ac.tuwien.aic13g3t2.dao;

import ac.tuwien.aic13g3t2.entity.Article;


public interface IArticleDAO extends IGenericDAO<Article, Long> {

	public boolean isExisting(String title);
	
}
