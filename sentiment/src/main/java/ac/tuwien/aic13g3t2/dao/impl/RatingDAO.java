package ac.tuwien.aic13g3t2.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ac.tuwien.aic13g3t2.dao.IRatingDAO;
import ac.tuwien.aic13g3t2.entity.Rating;
import ac.tuwien.aic13g3t2.entity.SentimentRating;

@Repository
public class RatingDAO extends HibernateDAO<Rating, Long> implements IRatingDAO {
	
	
	public List<SentimentRating> getAvgRatingsByKeyword(String keyword) {
		List<SentimentRating> sentimentRatings = null;
		//		return ((Number) currentSession().createCriteria(Mention.class).add(Restrictions.eq("article", article)).add(Restrictions.eq("keyword", keyword)).setProjection(Projections.rowCount()).uniqueResult()).intValue() > 0;

		//return currentSession().createCriteria(Rating.class).add(Restrictions.e).list();
		
		Query query = currentSession().createSQLQuery(
				"select avg(Case " +
					"when rating.rating='very good' then 1 " +
					"when rating.rating='good' then 2 " +
					"when rating.rating='neutral' then 3 " +
					"when rating.rating='bad' then 4 " +
					"when rating.rating='very bad' then 5 " +
					"End) as sentimentByArticle, article.publicationDate, article.url, rating.rating " +
					"from rating, mention, keyword, article " +
						"where rating.mention_mentionID = mention.mentionID and " +
						"mention.article_id = article.id and " +
						"mention.keyword_keywordID = keyword.keywordID and " +
						"keyword.keyword like :kword " +
						"group by rating.mention_mentionID " +
						"having (Select count(*) from rating x " +
							"where x.mention_mentionID = rating.mention_mentionID and " +
							"x.rating != '' or x.rating != null " +
							"group by x.mention_mentionID) = :targetratingcount").setParameter("kword", keyword).setParameter("targetratingcount", Rating.TARGET_RATING_COUNT);
				
		List result = query.list();
		System.out.println("--------------------------");
		System.out.println("we have in total " + result.size() + " results");

		if (result.size() > 0) {
			sentimentRatings = new ArrayList<SentimentRating>();
			// create from the array of objects a list of SentimentRatings
			for (Object item : result) {
			    Object[] element = (Object[]) item; 
			    SentimentRating sentimentRating = new SentimentRating();
			    sentimentRating.setSentimentRating(new Double(((BigDecimal) element[0]).doubleValue()));
			    sentimentRating.setDate((Date)element[1]);
			    sentimentRating.setArticleURL((String)element[2]);
			    sentimentRatings.add(sentimentRating);
			}
			System.out.println("--------------------------");
			System.out.println(sentimentRatings.toString());
		}
		return sentimentRatings;
	}
	
	public Double getOverallAvgRatingsByKeyword(String keyword) {
		Double overallRating = null;
		
		Query query = currentSession().createSQLQuery(
				"select avg(Case " +
					"when rating.rating='very good' then 1 " +
					"when rating.rating='good' then 2 " +
					"when rating.rating='neutral' then 3 " +
					"when rating.rating='bad' then 4 " +
					"when rating.rating='very bad' then 5 " +
					"End) as sentimentByArticle " +
					"from rating, mention, keyword " +
						"where rating.mention_mentionID = mention.mentionID and " +
						"mention.keyword_keywordID = keyword.keywordID and " +
						"keyword.keyword like :k and " +
						"rating.mention_mentionID in ( " +
							"select distinct x.mention_mentionID from rating x " +
							"having (Select count(*) from rating y " + 
							"where y.mention_mentionID = x.mention_mentionID and y.rating != '' or y.rating != null group by y.mention_mentionID) = :targetratingcount)"
		).setParameter("k", keyword).setParameter("targetratingcount", Rating.TARGET_RATING_COUNT);
		
		System.out.println("--------------------------");
		List result = query.list();
		System.out.println(result);
		
		// dirty code, it was late ... get the first object from list, cast it to an array and get the first array field to get the number there :D
		if (result != null && result.size() == 1 && result.get(0) != null) {
			overallRating = new Double(((BigDecimal) result.get(0)).doubleValue());
		}
		
		return overallRating;
	}

	@Override
	public List<Rating> getRatingsForUser(Integer userID) {
		return currentSession().createCriteria(Rating.class).add(Restrictions.eq("userID", userID)).list();
	}

	@Override
	public Integer getRatingCountForUser(Integer userID) {
		return currentSession().createCriteria(Rating.class).add(Restrictions.eq("userID",userID)).list().size();
	}

	@Override
	public List<Integer> getUserListForRatings() {
		return currentSession().createCriteria(Rating.class).add(Restrictions.isNotNull("rating")).setProjection(Projections.distinct( Projections.property( "userID" ))).list();
	}

	@Override
	public List<Rating> getRatingsForMention(Long mentionID) {
		return currentSession().createCriteria(Rating.class).add(Restrictions.isNotNull("rating")).add(Restrictions.eq("mention.mentionID", mentionID)).list();
	}

	@Override
	public List<Rating> getUnratedRatings(Long hours) {
		return currentSession().createCriteria(Rating.class)
				.add(Restrictions.isNull("rating")).list();
	}

}
