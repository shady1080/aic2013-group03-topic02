package ac.tuwien.aic13g3t2.service.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ac.tuwien.aic13g3t2.dao.IArticleDAO;
import ac.tuwien.aic13g3t2.dao.IKeywordDAO;
import ac.tuwien.aic13g3t2.dao.IMentionDAO;
import ac.tuwien.aic13g3t2.dao.IRatingDAO;
import ac.tuwien.aic13g3t2.entity.Article;
import ac.tuwien.aic13g3t2.entity.Keyword;
import ac.tuwien.aic13g3t2.entity.Mention;
import ac.tuwien.aic13g3t2.entity.MentionDetail;
import ac.tuwien.aic13g3t2.entity.MentionDetailDay;
import ac.tuwien.aic13g3t2.entity.Rating;
import ac.tuwien.aic13g3t2.entity.Sentiment;
import ac.tuwien.aic13g3t2.entity.SentimentHistory;
import ac.tuwien.aic13g3t2.entity.SentimentRating;
import ac.tuwien.aic13g3t2.entity.Task;
import ac.tuwien.aic13g3t2.service.IMentionService;
import ac.tuwien.aic13g3t2.util.Helper;
import ac.tuwien.aic13g3t2.util.Http;
import ac.tuwien.aic13g3t2.util.Settings;
import ac.tuwien.aic13g3t2.util.Settings.SettingsKeys;

@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
@Service("mentionService")
public class MentionService implements IMentionService{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
    private IArticleDAO articleDAO;
	
	@Autowired
    private IMentionDAO mentionDAO;

	@Autowired
    private IKeywordDAO keywordDAO;

	@Autowired
    private IRatingDAO ratingDAO;
	
	private static final Logger log = LoggerFactory.getLogger(MentionService.class);
		
	public void detect() {
		System.out.println("Searching for mentions...");
		
		List<Article> articles = articleDAO.list();

		System.out.println("Fetched " + articles.size() + " articles.");
		
		List<Keyword> keywords = keywordDAO.list();

		System.out.println("Fetched " + keywords.size() + " keywords.");
		
		int addedMentions = 0;

		for(Keyword keyword : keywords) {
			System.out.print(keyword.getKeyword() + ": ");
			for(Article article : articles) {
				if(article.getTitle().toLowerCase().contains(keyword.getKeyword().toLowerCase()) || article.getText().toLowerCase().contains(keyword.getKeyword().toLowerCase())) {
					if(mentionDAO.isExisting(article, keyword)) {
						System.out.print(".");
					}
					else {
						Mention mention = new Mention();
						mention.setArticle(article);
						mention.setKeyword(keyword);
						mentionDAO.add(mention);
						System.out.print("+");
						addedMentions++;
					}
				}
			}
			System.out.print("\n");
		}
		
		System.out.println("Mentions search complete. Found " + addedMentions + " new mentions.");
		
		System.out.println("Starting rating creation...");
		
		List<Mention> unratedMentions = mentionDAO.listUnrated();
		
		//System.out.println("Found " + unratedMentions.size() + " mentions with insufficient rating count (< " + Rating.TARGET_RATING_COUNT + ").");
		System.out.println("Found " + unratedMentions.size() + " mentions.");
		
		int addedRatings = 0;
		for(Mention mention : unratedMentions) {
			int currentCount = mention.getRatings().size();
			while(currentCount < Rating.TARGET_RATING_COUNT) {
				Rating rating = new Rating();
				rating.setMention(mention);
				ratingDAO.add(rating);
				rating.publish();
				currentCount += 1;
				addedRatings += 1;
				System.out.print("+");
			}
			System.out.print("\n");
		}
		
		System.out.println("Added " + addedRatings + " ratings.");
		
	}

	// this is the old evaluate function, calculating the average of all 3 ratings of a mention
	@Override
	public Sentiment evaluateAvg(String keyword) {
		Sentiment sentiment = null;
		System.out.println("Searching for keyword: " + keyword);
		
		Keyword k = keywordDAO.find(keyword);
		if (k != null) {
			System.out.println("found keyword: " + k.toString());
			// create our Sentiment object
			sentiment = new Sentiment();
			
			// set the list of ratings, one rating per article
			sentiment.setSentimentRating(ratingDAO.getAvgRatingsByKeyword(keyword));
			
			// set the overall sentiment rating
			if (ratingDAO.getOverallAvgRatingsByKeyword(keyword) == null) {
				System.out.println("ERROR! no rating yet for keyword '" + keyword + "'");
				return null;
			}
			sentiment.setTotalSentiment(ratingDAO.getOverallAvgRatingsByKeyword(keyword));
			
			// normalize ratings from 1 to 5 into -1 to +1
			normalizeSentiment(sentiment);
		}
		else {
			System.out.println("ERROR! keyword " + keyword + " has not been found!");
			return null;
		}
		return sentiment;		
	}
	
	@Override
	public Sentiment evaluate(String keyword) {
		Sentiment sentiment = null;
		List<SentimentRating> sentimentRatingList = null;
		List<SentimentHistory> sentimentHistoryList = null;
		List<MentionDetail> mentionDetailList = new ArrayList<MentionDetail>();
		List<MentionDetailDay> mentionDetaiDaylList = new ArrayList<MentionDetailDay>();
		
		Double overallRating = null;
		
		System.out.println("Evaluating mentions for keyword: " + keyword);
		
		Keyword k = keywordDAO.find(keyword);
		if (k != null) {
			System.out.println("Found keyword: " + k.toString());
			
			// get all mentions with the keyword k
			List<Mention> mentionlist = findByKeyword(k);
			System.out.println("Found " + mentionlist.size() + " mentions with keyword " + keyword + ":\n" + mentionlist);
			
			// loop through all the mentions
			for(Mention mention: mentionlist) {
			
				// get ratings for the mentions
				List<Rating> ratingsOfMention = ratingDAO.getRatingsForMention(mention.getMentionID());
				
				// calculate stuff only if there are Rating.TARGET_RATING_COUNT ratings (default 3)
				if(ratingsOfMention.size() == Rating.TARGET_RATING_COUNT) {
					MentionDetail mentiondetail = new MentionDetail();
					System.out.println("Found " + Rating.TARGET_RATING_COUNT + " Ratings for Mention " + mention);
					mentiondetail.setMention(mention);

					// calculate mean
					double mean = calculateMean(ratingsOfMention);
					System.out.println("mean for mentionID " + mention.getMentionID() + " is " + mean);
					mentiondetail.setMean(mean);
					
					// calculate sample variance s^2 = 1/(n-1) sum((x_i - mean)^2)
					double sampleVariance = calculateSampleVariance(ratingsOfMention, mean);
					System.out.println("sample variance s^2 for mentionID " + mention.getMentionID() + " is " + sampleVariance);
					mentiondetail.setSampleVariance(sampleVariance);
					
					// calculate standard deviation s = sqrt(sample variance s^2);
					double standardDeviation = Math.sqrt(sampleVariance);
					System.out.println("standard deviation for mentionID " + mention.getMentionID() + " is " + standardDeviation);
					mentiondetail.setStandardDeviation(standardDeviation);
					
					// add the mentiondetail to the list
					mentionDetailList.add(mentiondetail);
				}			
			}
			
			System.out.println("Horray, we have " + mentionDetailList.size() + " mentions with 3 ratings!");
			System.out.println("mentionDetailList: " + mentionDetailList);
			
			// check if we have any mentions with 3 ratings at all, if yes, then start the calculation for the overall sentiment per article
			if (mentionDetailList.size() > 0) {
				// get the maximum standard deviation
				MentionDetail s_max = Collections.max(mentionDetailList);
				System.out.println("Max standard deviation is: " + s_max.getStandardDeviation());
				
				// now calculate t_i = (s_max - s)/s_max for all mentionDetails
				// and sum up all t_i to a overall t_all sum
				double t_all = 0.0;
				for (MentionDetail mentionDetail: mentionDetailList) {
					double t_i = (s_max.getStandardDeviation() - mentionDetail.getStandardDeviation()) / s_max.getStandardDeviation();
					//System.out.println("t_i for mentionID "  + mentionDetail.getMention().getMentionID() + " is " + t_i);
					mentionDetail.setT_i(t_i);
					t_all = t_all + t_i;
				}
				System.out.println("t_all for all mentions is: " + t_all);
	
				// now calculate the overall rating for the keyword over all mention-ratings by article
				/*
				double overallRatingDoubleValue = 0.0;
				for (MentionDetail mentionDetail: mentionDetailList) {
					overallRatingDoubleValue = overallRatingDoubleValue + (mentionDetail.getMean() * mentionDetail.getT_i());
				}
				overallRatingDoubleValue = overallRatingDoubleValue / t_all;
				System.out.println("Overall rating is: " + overallRatingDoubleValue);
				overallRating = new Double(overallRatingDoubleValue);
				*/
				
				// get the details for the sentimentRatings
				sentimentRatingList = new ArrayList<SentimentRating>();
				for (MentionDetail mentionDetail: mentionDetailList) {
					SentimentRating sentimentRating = new SentimentRating();
					sentimentRating.setArticleURL(mentionDetail.getMention().getArticle().getUrl());
					sentimentRating.setDate(mentionDetail.getMention().getArticle().getPublicationDate());
					sentimentRating.setSentimentRating(mentionDetail.getMean());
					sentimentRatingList.add(sentimentRating);
				}
			}
			
			System.out.println("---------------------");
			System.out.println("aggregate the mentions for a full day");
			System.out.println("---------------------");
			
			// calculate overall ratings per day
			// in the mendionDetailList we have all mentions with 3 ratings for the keyword
			// we have to aggregate them to groups of ratings per day
			
			// aggregate from the mentionDetailList (the list of mentions by articles) our list of mentions by date
			// loop trough the mentionDetailList
			for (MentionDetail mentionDetail: mentionDetailList) {
				Mention mention = mentionDetail.getMention();
				Date mentionDate = mention.getArticle().getPublicationDate();
				System.out.println("Working on MentionDetail: " + mention + " " + mentionDate);
				
				// check if there is a mention day with the specific date
				// if yes, then add the ratings to the list, if not create one
				int mentionDetailDayIndex = getMentionDetailDayByDate(mentionDetaiDaylList, mentionDate);
				
				if (mentionDetailDayIndex == -1 ) {
					// we have to create a new mentionDetailDay
					MentionDetailDay myNewMentionDetailDay = new MentionDetailDay();
					myNewMentionDetailDay.setDate(new Date(mentionDate.getYear(), mentionDate.getMonth(), mentionDate.getDate()));
					List<Mention> myNewMentionList = new ArrayList<Mention>();
					System.out.println("Mention to add is " + mention);
					myNewMentionList.add(mention);
					myNewMentionDetailDay.setMentionList(myNewMentionList);
					
					// finally add the new MentionDetailDay to the list
					mentionDetaiDaylList.add(myNewMentionDetailDay);
					System.out.println("created new mentionDetailDay: " + myNewMentionDetailDay);
				}
				else {
					// just add the ratings to the mentionDetailDay
					mentionDetaiDaylList.get(mentionDetailDayIndex).getMentionList().add(mention);
					System.out.println("updated mentionDetailDay: " + mentionDetaiDaylList.get(mentionDetailDayIndex));
				}
				
			}
			
			System.out.println("Finally the full list of days of mentions, we have: " + mentionDetaiDaylList.size() + " days");
			// calculation makes only sense if we have something to calculate :D
			if (mentionDetaiDaylList.size() > 0 ) {
				for (MentionDetailDay mentionDetailDay: mentionDetaiDaylList) {
					System.out.println("on " + mentionDetailDay.getDate() + " there are " + mentionDetailDay.getMentionList().size() + " ratings");
				}
				
				// loop through our mentionDetailDayList and calculate everything
				for (MentionDetailDay mentionDetailDay: mentionDetaiDaylList) {
					// calculate mean
					
					System.out.println("--- Calculatin stuff for date " + mentionDetailDay.getDate());
					double mean = calculateMean(mentionDetailDay.getAllRatingsForMentions());
					System.out.println("mean for date " + mentionDetailDay.getDate() + " is " + mean);
					mentionDetailDay.setMean(mean);
					
					// calculate sample variance s^2 = 1/(n-1) sum((x_i - mean)^2)
					double sampleVariance = calculateSampleVariance(mentionDetailDay.getAllRatingsForMentions(), mean);
					System.out.println("sample variance s^2 for date " + mentionDetailDay.getDate() + " is " + sampleVariance);
					mentionDetailDay.setSampleVariance(sampleVariance);
					
					// calculate standard deviation s = sqrt(sample variance s^2);
					double standardDeviation = Math.sqrt(sampleVariance);
					System.out.println("standard deviation for date " + mentionDetailDay.getDate() + " is " + standardDeviation);
					mentionDetailDay.setStandardDeviation(standardDeviation);
				}
				
				// get the maximum standard deviation
				MentionDetailDay s_max_day = Collections.max(mentionDetaiDaylList);
				System.out.println("Max standard deviation for the days: " + s_max_day.getStandardDeviation());
				
				// now calculate t_i = (s_max - s)/s_max for all mentionDetailDays
				// and sum up all t_i to a overall t_all sum
				double t_all_day = 0.0;
				for (MentionDetailDay mentionDetailDay: mentionDetaiDaylList) {
					double t_i_day = (s_max_day.getStandardDeviation() - mentionDetailDay.getStandardDeviation()) / s_max_day.getStandardDeviation();
					System.out.println("t_i for date "  + mentionDetailDay.getDate() + " is " + t_i_day);
					mentionDetailDay.setT_i(t_i_day);
					t_all_day = t_all_day + t_i_day;
				}
				System.out.println("t_all_day for all days is: " + t_all_day);
	
				// now calculate the overall rating for the keyword over all mention-ratings by date
				double overallDateRatingDoubleValue = 0.0;
				for (MentionDetailDay mentionDetailDay: mentionDetaiDaylList) {
					overallDateRatingDoubleValue = overallDateRatingDoubleValue + (mentionDetailDay.getMean() * mentionDetailDay.getT_i());
				}
				overallDateRatingDoubleValue = overallDateRatingDoubleValue / t_all_day;
				System.out.println("Overall date rating is: " + overallDateRatingDoubleValue);
				overallRating = new Double(overallDateRatingDoubleValue);
				
				// create the final list of history entries, one entry per day,
				// if we have no day for the mentions, create an empty SentimentHistory object
				sentimentHistoryList = new ArrayList<SentimentHistory>();
				
				
				// first get the oldest mention with 3 ratings
				Mention oldestMention = getOldestMention(mentionDetailList);
				Date oldestDate = oldestMention.getArticle().getPublicationDate();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
				// create a calendar object from the date of the first mention
				// handling date stuff in Java is a pain in the ass :(
				
				// get a dummy object to convert the Date object into a Calendar object
				Calendar xxx = Calendar.getInstance();
				xxx.clear();
				xxx.setTime(oldestDate);
				// create from the dummy Calendar object the final Calendar object, we need only the date (year, month, day)
				Calendar oldestDay = Calendar.getInstance();
				oldestDay.clear();
				//oldestDay.set(oldestDate.getYear(), oldestDate.getMonth()-1, oldestDate.getDate());
				oldestDay.set(Calendar.YEAR, xxx.get(Calendar.YEAR));
				oldestDay.set(Calendar.MONTH, xxx.get(Calendar.MONTH));
				oldestDay.set(Calendar.DAY_OF_MONTH, xxx.get(Calendar.DAY_OF_MONTH));
				System.out.println("oldest mention date is: " + sdf.format(oldestDay.getTime()));
				
				// get the current date in a Calendar object
				Calendar today = Calendar.getInstance();
				today.clear();
				today.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR));
				today.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH));
				today.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
				
				// start from the oldest date and generate for every day a SentimentHistory until today
				// save the mean for days where we have no value
				Double oldMean = null;
				for(Calendar c = oldestDay; c.compareTo(today) < 0; oldestDay.add(Calendar.DAY_OF_MONTH, 1)) {
					SentimentHistory sentimentHistory = new SentimentHistory();
					Date date = c.getTime();				
					sentimentHistory.setDate(date);
					
					// loop through all days and check if we have a calculated value for the day
					for (MentionDetailDay mentionDetailDay: mentionDetaiDaylList) {
						if (mentionDetailDay.getDate().equals(date)) {
							oldMean = mentionDetailDay.getMean();
						}
					}
					
					sentimentHistory.setSentimentRating(oldMean);
					sentimentHistoryList.add(sentimentHistory);
				}
			
			}
			// create our Sentiment object and fill the object with data
			sentiment = new Sentiment();
			sentiment.setTotalSentiment(overallRating);
			sentiment.setSentimentRating(sentimentRatingList);
			sentiment.setSentimentHistory(sentimentHistoryList);

		}
		else {
			System.out.println("ERROR! keyword " + keyword + " has not been found!");
			return null;
		}
		
		return sentiment;
	}
	
	// helper function, getting the oldest mention (oldest article with the mention) from the mention detail list
	private Mention getOldestMention(List<MentionDetail> mentionDetailList) {
		Mention oldestMention = mentionDetailList.get(0).getMention();

		for (MentionDetail mentionDetail: mentionDetailList) {
			//System.out.println(mentionDetail.getMention().getArticle().getPublicationDate() + " before " + oldestMention.getArticle().getPublicationDate() + "?");
						
			if (mentionDetail.getMention().getArticle().getPublicationDate().before(oldestMention.getArticle().getPublicationDate())) {
				//System.out.println("setting oldestMention to MentionID: " + mentionDetail.getMention().getMentionID() + " from " + mentionDetail.getMention().getArticle().getPublicationDate());
				oldestMention = mentionDetail.getMention();
			}
		
		}
		System.out.println("oldest mention is MentionID: " + oldestMention.getMentionID() + " from " + oldestMention.getArticle().getPublicationDate());
		
		return oldestMention;
	}

	// calculate the mean of a list of ratings
	private double calculateMean(List<Rating> ratingsOfMention) {
		double mean = 0.0;
		double sum = 0.0;
		
		for(Rating rating : ratingsOfMention){
			sum = sum + getDoubleRating(rating);
		}
		
		mean = sum / ratingsOfMention.size();
				
		return mean;
	}
	
	// calculate the sample mean of a list of ratings
	private double calculateSampleVariance(List<Rating> ratingsOfMention, double mean) {
		double sampleVariance = 0.0;
		double sum = 0.0;
		//System.out.println("mean: " + mean);
		
		// sample variance s^2 = 1/(n-1) sum((x_i - mean)^2)
		for(Rating rating : ratingsOfMention){
			//System.out.println("Sum: " + sum);
			double x = getDoubleRating(rating);
			//System.out.println("x: " + x);
			//System.out.println("(x - mean): " + (x - mean));
			//System.out.println("(x - mean) * (x - mean): " + (x - mean) * (x - mean));
			sum = sum + ((x - mean) * (x - mean));
		}
		//System.out.println("Sum after loop: " + sum);
		//System.out.println("(ratingsOfMention.size()): " + ratingsOfMention.size());
		//System.out.println("(ratingsOfMention.size() - 1.0): " + (ratingsOfMention.size() - 1.0));
		//System.out.println("(1.0 / (ratingsOfMention.size() - 1.0)): " + (1.0 / (ratingsOfMention.size() - 1.0)));
		sampleVariance = (1.0 / (ratingsOfMention.size() - 1.0)) * sum;
		
		//System.out.println("sampleVariance: " + sampleVariance);
		
		return sampleVariance;
	}
	
	// returns a double value between -1 and +1 for the textual rating
	private double getDoubleRating(Rating rating) {
		
		String stringRating = rating.getRating();
		
			
		if(stringRating.equals("very good")) 	return 1.0;
		if(stringRating.equals("good")) 		return 0.5;
		if(stringRating.equals("neutral")) 		return 0.0;
		if(stringRating.equals("bad")) 			return -0.5;
		if(stringRating.equals("very bad")) 	return -1.0;
		
		
		/*
		if(stringRating.equals("very good")) 	return 1.0;
		if(stringRating.equals("good")) 		return 2.0;
		if(stringRating.equals("neutral")) 		return 3.0;
		if(stringRating.equals("bad")) 			return 4.0;
		if(stringRating.equals("very bad")) 	return 5.0;
		*/
		// what to do if the rating has a different rating text? should never happen, ha :D
		return 0;
	}

	// compares 2 dates ignoring the time
	// the value 0 if the time represented by the argument is equal to the time represented 
	// a value less than 0 if the time of this Calendar is before the time represented by the argument;
	// and a value greater than 0 if the time of this Calendar is after the time represented by the argument.
	@SuppressWarnings("deprecation")
	private int compareDates(Date d1, Date d2) {
		Calendar c1_temp = Calendar.getInstance();
		c1_temp.clear();
		c1_temp.setTime(d1);
		
		Calendar c1 = Calendar.getInstance();
		c1.clear();
		c1.set(Calendar.YEAR, c1_temp.get(Calendar.YEAR));
		c1.set(Calendar.MONTH, c1_temp.get(Calendar.MONTH));
		c1.set(Calendar.DAY_OF_MONTH, c1_temp.get(Calendar.DAY_OF_MONTH));
		
		Calendar c2_temp = Calendar.getInstance();
		c2_temp.clear();
		c2_temp.setTime(d2);
		
		Calendar c2 = Calendar.getInstance();
		c2.clear();
		c2.set(Calendar.YEAR, c2_temp.get(Calendar.YEAR));
		c2.set(Calendar.MONTH, c2_temp.get(Calendar.MONTH));
		c2.set(Calendar.DAY_OF_MONTH, c2_temp.get(Calendar.DAY_OF_MONTH));
		
		return c1.compareTo(c2);
	}
	
	private int getMentionDetailDayByDate(List<MentionDetailDay> mentionDetaiDaylList, Date mentionDate) {
		
		for (int i = 0; i < mentionDetaiDaylList.size(); i++) {
			MentionDetailDay m = mentionDetaiDaylList.get(i);
			if (compareDates(m.getDate(), mentionDate) == 0) {
				return i;
			}
		}
		
		return -1;
	}
	
	@Override
	public void add(Mention entity) {
		// TODO do we need this?
	}

	@Override
	public void update(Mention entity) {
		// TODO do we need this?
	}

	@Override
	public void remove(Mention entity) {
		// TODO do we need this?
	}

	@Override
	public Mention find(Long key) {
		// TODO do we need this?
		return null;
	}

	@Override
	public List<Mention> list() {
		return mentionDAO.list();
	}
	
	private Sentiment normalizeSentiment(Sentiment s) {
		
		// normalize only objects that are not null ;)
		if (s != null) {
			// normalize the the overall sentiment rating
			Double overallSentimentValue = 1.5 - (s.getTotalSentiment()/2);
			System.out.println("normalized overall sentiment value " + s.getTotalSentiment() + " to => " + overallSentimentValue);
			s.setTotalSentiment(overallSentimentValue);
	
			for (Iterator<SentimentRating> iterator = s.getSentimentRating().iterator(); iterator.hasNext();) {
				SentimentRating sr = iterator.next();
				Double sentimentValue = 1.5 -(sr.getSentimentRating()/2);
				System.out.println("normalized sentiment value " + sr.getSentimentRating() + " to => " + sentimentValue);
				sr.setSentimentRating(sentimentValue);
			}
		}
		
		return s;
	}

	@Override
	public void reprice(Long maxHours, Double maxReward) {
		try {
			// store to settings.xml
			Settings.store(SettingsKeys.MAXHOURS, maxHours+"");
			Settings.store(SettingsKeys.MAXREWARD, maxReward+"");		
			
			// get map of all undone tasks from crowdsourcing-server
			String taskPrices = Http.get(Settings.CROWDSOURCEURL + "/undonetasks");
			Map<Long,Task> tasks = new ObjectMapper().readValue(taskPrices, new TypeReference<HashMap<Long,Task>>(){});
			
			// search for old ratings
			List<Rating> unratedRatings = ratingDAO.getUnratedRatings(maxHours);
			log.info("Now repricing undone tasks..");
			
			for (Rating r : unratedRatings) {
				System.out.print("+");
				if (r.getCreationDate() != null
						&& r.getCreationDate() < System.currentTimeMillis() - maxHours * 3600000) { // creation date is older than allowed		
					if (tasks.containsKey(r.getRatingID())) { // the corresponding task exists on the crowdsourcing-server
						Task t = tasks.get(r.getRatingID());
						Double newPrice = Helper.round(t.getPrice() * 1.25, 2);
						if (newPrice < maxReward) { // update task price
							Http.put(Settings.CROWDSOURCEURL + "/tasks/" + t.getTaskID(), newPrice + "");
							//log.info("UPDATE " + t);
						}
						else { // delete task from crowdsourcing-server
							Http.delete(Settings.CROWDSOURCEURL + "/tasks/" + t.getTaskID());
							//log.info("DELETE " + t);
						}
					} else { // the corresponding task does NOT exists on the crowdsourcing-server
						//log.info("CREATE Task from Rating " + r);
						r.publish();
					}
					
				}
			} System.out.println(" Finished.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<Mention> findByKeyword(Keyword keyword) {
		return mentionDAO.findByKeyword(keyword);
	}

	@Override
	public List<Mention> findByArticle(Article article) {
		return mentionDAO.findByArticle(article);
	}

}
