package ac.tuwien.aic13g3t2.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ac.tuwien.aic13g3t2.dao.IKeywordDAO;
import ac.tuwien.aic13g3t2.entity.Keyword;
import ac.tuwien.aic13g3t2.service.IKeywordsService;

@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
@Service("keywords")
public class KeywordsService implements IKeywordsService{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
    private IKeywordDAO keywordDAO;

	@Override
	@PostConstruct
	public void readKeywords() {
		keywordDAO.readKeywords();	
		System.out.println("ReadKeywordsService - found " + keywordDAO.list().size() + " keywords in the database!");
	}

	@Override
	public void add(Keyword entity) {
		keywordDAO.add(entity);
	}

	@Override
	public void update(Keyword entity) {
		keywordDAO.update(entity);
	}

	@Override
	public void remove(Keyword entity) {
		keywordDAO.remove(entity);
	}

	@Override
	public Keyword find(Long key) {
		return keywordDAO.find(key);
	}

	@Override
	public Keyword find(String keyword) {
		return keywordDAO.find(keyword);
	}

	@Override
	public List<Keyword> list() {
		return keywordDAO.list();
	}

	
	
}
