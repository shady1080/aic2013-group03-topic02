package ac.tuwien.aic13g3t2.service;

import ac.tuwien.aic13g3t2.entity.Article;

public interface IArticleService extends IGenericService<Article, Long> {
	public void extract();
}
