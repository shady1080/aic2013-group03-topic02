package ac.tuwien.aic13g3t2.service;

import ac.tuwien.aic13g3t2.entity.Rating;


public interface ICallbackService{ //extends IGenericService<Article, Long> {
	public void callback(Long ratingID, Rating rating);

}
