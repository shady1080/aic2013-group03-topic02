package ac.tuwien.aic13g3t2.service.impl;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ac.tuwien.aic13g3t2.dao.IRatingDAO;
import ac.tuwien.aic13g3t2.entity.Rating;
import ac.tuwien.aic13g3t2.service.ICallbackService;

@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
@Service("callback")
public class CallbackService implements ICallbackService{
	
	private static final Logger logger = LoggerFactory.getLogger(BadUserService.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
    private IRatingDAO ratingDAO;
	
	@Override
	public void callback(Long ratingID, Rating rating) {
		
		Rating trating = ratingDAO.find(ratingID);
		if(trating != null){
			
			trating.setRating(rating.getRating());
			trating.setUserID(rating.getUserID());
			
			ratingDAO.update(trating);
			
			logger.info("User "+rating.getUserID()+" ratet ratingID " + rating.getRating()+  " with "+ trating.getRating());
		}
		else{
			logger.info("RatingID " + ratingID + " not found");
		}
			
		
	}
	
}
