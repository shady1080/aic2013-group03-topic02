package ac.tuwien.aic13g3t2.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ac.tuwien.aic13g3t2.dao.IMentionDAO;
import ac.tuwien.aic13g3t2.dao.IRatingDAO;
import ac.tuwien.aic13g3t2.entity.Rating;
import ac.tuwien.aic13g3t2.entity.UserDA;
import ac.tuwien.aic13g3t2.service.IBadUserService;
import ac.tuwien.aic13g3t2.util.Http;
import ac.tuwien.aic13g3t2.util.Settings;

@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
@Service("badUserService")
public class BadUserService implements IBadUserService{
	
	private final double TRESHOLD = 0.85;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
    private IRatingDAO ratingDAO;
	private IMentionDAO mentionDAO;
	
	private static final Logger logger = LoggerFactory.getLogger(BadUserService.class);

	@Override
	public void findBadUsers() {
		logger.info("Starting to find bad Users");
		
		List<Integer> users = ratingDAO.getUserListForRatings();
		users = deleteAllreadyBlocked(users);
		
		//Iterate over all Users which had done a rating
		for(Integer userID: users){
			
			if(userID != null){	
				logger.info("User: "+userID);
				//get count of total ratings for user
				Integer ratingCount = ratingDAO.getRatingCountForUser(userID);
				//just if user has more than 10 ratings done, check if he is "bad"
				if( ratingCount > 10){
					//read all Ratings where the user in involved
					List<Rating> ratings = ratingDAO.getRatingsForUser(userID);
					logger.info("\t# Ratings: "+ratings.size());
					//times where user was misfit in the group-rating
					Integer outtakes=0;
					for(Rating rating: ratings){
						//read all Ratings (3-Group) for the current Rating of the user
						List<Rating> ratingsGroup = ratingDAO.getRatingsForMention(rating.getMention().getMentionID());		
						if(ratingsGroup.size() == Rating.TARGET_RATING_COUNT){
							double mean = meanValue(ratingsGroup);
							double sampleMean = sampleMeanValue(ratingsGroup, mean);
							double standardDev =  standardDeviation(sampleMean);
							
							//if standard Deviation is > TRESHOLD continue
							if(standardDev > TRESHOLD){
								logger.info("\t# Standard Deviation > : "+TRESHOLD);
								double r = transalteRating(rating.getRating());
								//if the rating of the user is out of range increase outtakes
								if(r < (mean-standardDev) || r > (mean+standardDev)){
									logger.info("\tUser with ID "+userID+" is missfitting for mention "+rating.getMention().getMentionID());
									outtakes++;
								}
							}
						}						
					}
					//if the user was more than 50% misfitting in a group-rating block the user
					if(((double)outtakes/(double)ratings.size()) > 0.25d){
						logger.info("\tBlock user with ID "+userID);
						blockUser(userID);
					}	
				}
			}				
		}	
	}
	
	private double transalteRating(String rating){
		
		if(rating.equals("very good")) return 1.0;
		if(rating.equals("good")) return 0.5;
		if(rating.equals("neutral")) return 0.0;
		if(rating.equals("bad")) return -0.5;
		if(rating.equals("very bad")) return -1.0;
		
		return -2;
	}

	private List<Integer> deleteAllreadyBlocked(List<Integer> users){
		
		List<UserDA> bUsers =getBlockedUsers();
		
		
		for(UserDA u: bUsers){
			users.remove(Integer.valueOf(u.getId().intValue()));	
		}
		
		return users;
	}
	
	private void blockUser(Integer userID){
		try {
			Http.post(Settings.CROWDSOURCEURL+"/bans/"+userID);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private double meanValue(List<Rating> ratings){
		
		double mean =0.0;
		
		for(Rating rating : ratings){
			double r = transalteRating(rating.getRating());
			if(r > -2){
				mean += r;
			}
		}
		
		return mean/ratings.size();
	}
	
	private double sampleMeanValue(List<Rating> ratings, double mean){
		
		double sampleMean =0.0;
		
		for(Rating rating : ratings){
			double r = transalteRating(rating.getRating());
			if(r > -2){
				sampleMean += (r - mean) * (r - mean);
			}
		}
		
		return sampleMean/(ratings.size()-1);
	}
	
	private double standardDeviation(double sampleMean){
		return Math.sqrt(sampleMean);
	}
	
	public List<UserDA> getBlockedUsers(){
		List<UserDA> bUsers = new ArrayList<UserDA>();
		
		String urlString = Settings.CROWDSOURCEURL+"/bans";
		URL url;
		try {
			url = new URL(urlString);
			URLConnection conn = url.openConnection();
			InputStream is = conn.getInputStream();
			
			ObjectMapper mapper = new ObjectMapper(); 
			bUsers = mapper.readValue(is , mapper.getTypeFactory().constructCollectionType(List.class, UserDA.class));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return bUsers;
	}
	

}
