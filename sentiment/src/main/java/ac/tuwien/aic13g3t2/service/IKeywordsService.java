package ac.tuwien.aic13g3t2.service;

import ac.tuwien.aic13g3t2.entity.Keyword;


public interface IKeywordsService  extends IGenericService<Keyword, Long> {
	public void readKeywords();

	public Keyword find(String key);
}
