package ac.tuwien.aic13g3t2.service.impl;

import it.sauronsoftware.feed4j.FeedIOException;
import it.sauronsoftware.feed4j.FeedParser;
import it.sauronsoftware.feed4j.FeedXMLParseException;
import it.sauronsoftware.feed4j.UnsupportedFeedException;
import it.sauronsoftware.feed4j.bean.Feed;
import it.sauronsoftware.feed4j.bean.FeedHeader;
import it.sauronsoftware.feed4j.bean.FeedItem;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ac.tuwien.aic13g3t2.dao.IArticleDAO;
import ac.tuwien.aic13g3t2.entity.Article;
import ac.tuwien.aic13g3t2.service.IArticleService;

@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
@Service("articleService")
public class ArticleService implements IArticleService{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
    private IArticleDAO articleDAO;
	
	public void extract() {
		try {
			String urlString = "http://finance.yahoo.com/news/?format=rss";
			
			System.out.println("Starting Yahoo Finance RSS Feed Extractor...");
			System.out.println("Source URL: " + urlString);

			URL url = new URL(urlString);
			
			System.out.print("Parsing...");

			Feed feed = FeedParser.parse(url);
			
			System.out.println(" done.");

			FeedHeader header = feed.getHeader();
			//System.out.println("Title: " + header.getTitle());
			//System.out.println("Link: " + header.getLink());
			//System.out.println("Description: " + header.getDescription());
			//System.out.println("Language: " + header.getLanguage());
			// System.out.println("PubDate: " + header.getPubDate());
			
			System.out.println("Fetched " + feed.getItemCount() + " RSS items.");

			int items = feed.getItemCount();
			for (int i = 0; i < items; i++) {
				FeedItem item = feed.getItem(i);
				
				String shortName = item.getTitle();
				if(shortName.length() > 18) {
					shortName = shortName.substring(0, 14) + "[...]";
				}
				
				System.out.print("Importing article '" + shortName + "' ...");
				if(articleDAO.isExisting(item.getTitle())) {
					System.out.println(" Skipped (already existing).");
				}
				else {
					Article article = new Article();
					article.setTitle(item.getTitle());
					article.setText(item.getDescriptionAsText());
					article.setUrl(item.getLink().toString());
					if(item.getPubDate() != null) {
						article.setPublicationDate(item.getPubDate());
					}
					else if(header.getPubDate() != null) {
						article.setPublicationDate(header.getPubDate());
					}
					articleDAO.add(article);
					System.out.println("Saved!");
				}
				
				System.out.println("Yahoo Finance RSS Feed Extractor finished successfully.");
				
				//System.out.println("Title: " + item.getTitle());
				//System.out.println("Link: " + item.getLink());
				//System.out.println("Plain text description: " + item.getDescriptionAsText());
				// System.out.println("HTML description: " +
				// item.getDescriptionAsHTML());
			}
		} catch (BeansException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FeedIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FeedXMLParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedFeedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void add(Article entity) {
		articleDAO.add(entity);
	}

	@Override
	public void update(Article entity) {
		articleDAO.update(entity);
	}

	@Override
	public void remove(Article entity) {
		articleDAO.remove(entity);
	}

	@Override
	public Article find(Long key) {
		return articleDAO.find(key);
	}

	@Override
	public List<Article> list() {
		return articleDAO.list();
	}
}
