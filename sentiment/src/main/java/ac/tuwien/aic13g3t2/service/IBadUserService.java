package ac.tuwien.aic13g3t2.service;

import java.util.List;

import ac.tuwien.aic13g3t2.entity.UserDA;


public interface IBadUserService  {
	public void findBadUsers();
	public List<UserDA> getBlockedUsers();
}
