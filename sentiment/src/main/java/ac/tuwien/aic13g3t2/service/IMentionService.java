package ac.tuwien.aic13g3t2.service;

import java.util.List;

import ac.tuwien.aic13g3t2.entity.Article;
import ac.tuwien.aic13g3t2.entity.Keyword;
import ac.tuwien.aic13g3t2.entity.Mention;
import ac.tuwien.aic13g3t2.entity.Sentiment;

public interface IMentionService extends IGenericService<Mention, Long> {
	public void detect();
	
	public Sentiment evaluateAvg(String keyword);
	
	public Sentiment evaluate(String keyword);

	public void reprice(Long maxHours, Double maxPrice);

	public List<Mention> findByKeyword(Keyword keyword);

	public List<Mention> findByArticle(Article article);
}
