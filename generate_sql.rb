#!/usr/bin/env ruby

# SQL random data generator script
#
# this script takes a list of rating IDs and their corresponding mention IDs
# as input and generates SQL UPDATE statements for the sentiment DB and the
# crowdsourcing DB.
#
# the script can be run with ruby (tested with ruby 2.0.0p353):
# $> ruby generate_sql.rb
#
# it will require you to copy some content of your mysql client over to it
#

#  ------------ CONFIG  ------------

# answer possibilities
options = [
  "very good",
  "good",
  "neutral",
  "bad",
  "very bad"
]

# each user has a tendency, which is either :balanced, :positive or :negative
# this affects to the random selection of his answers
# e.g. a positive tendency means that the user will never answer 'very bad'
users = [
  {:id => 100, :tendency => :balanced},
  {:id => 101, :tendency => :balanced},
  {:id => 102, :tendency => :balanced},

  {:id => 200, :tendency => :positive},

  {:id => 300, :tendency => :negative},
]

# ------------ END OF CONFIG ------------

puts "########################################"
puts "### SQL random data generator script ###"
puts "########################################"
puts
puts "Execute the following query and copy/paste the result."
puts "(then type ^D to submit pasted content)"
puts "  SELECT ratingID, mention_mentionID FROM rating WHERE rating IS NULL;"
print "> "

sql_output = ""
while !STDIN.eof?
  sql_output += STDIN.gets.chomp + "\n"
end

ratings = sql_output.scan(/^\|\s+([0-9]+)\s+\|\s+([0-9]+)\s+\|$/).map do |match|
  {
    :rating_id => match[0].to_i,
    :mention_id => match[1].to_i
  }
end

mentions = {}
ratings.each do |rating|
  rating_id = rating[:rating_id]
  mention_id = rating[:mention_id]
  mentions[mention_id] = [] unless mentions.key?(mention_id)
  mentions[mention_id] << rating_id
end

puts "Found #{ratings.size} ratings for #{mentions.size} mentions."

puts "--------------------"
puts "Generated SQL script below"
puts "--------------------"

sentiment_sqls = []
crowdsourcing_sqls = []

mentions.each do |mentionID, ratings|
  available_users = users.dup
  correct_rating = Random.rand(options.size)

  ratings.each do |rating|
    user = available_users[Random.rand(available_users.size)]
    available_users.delete(user)

    option_idx = if user[:tendency] == :positive
      0
    elsif user[:tendency] == :negative
      4
    else
      r = Kernel.rand
      if r > 0.66
        correct_rating
      elsif r > 0.33
        [correct_rating - 1, 0].max
      else
        [correct_rating + 1, options.size-1].min
      end
    end

    option = options[option_idx]
    sentiment_sqls << "UPDATE rating SET rating = '#{option}', userID = #{user[:id]} WHERE ratingID = #{rating};"
    crowdsourcing_sqls << "UPDATE tasks SET answer = '#{option}', obsolete = 1, userID = #{user[:id]} WHERE remoteID = #{rating};"
  end
end

puts "USE aic_sentiment;"
sentiment_sqls.each { |sql| puts sql }

puts "USE aic_crowdsourcing;"
crowdsourcing_sqls.each { |sql| puts sql }
